package dk.grinn.keycloak.migration.services;

/*-
 * #%L
 * Keycloak : Migrate : Model
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import java.util.UUID;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PessimisticLockException;

/**
 *
 * @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
public interface RealmHistoryLockService {

    /**
     * Abort any session on the realm.
     * <p>
     * Only to be used in an abnormal situation where the session id has been
     * lost (caused by an error on a previous run, etc.).
     *
     * @param realm the realm
     * @param abortedBy the user who aborts on the realm
     * @return 1 if session found
     */
    int abortSession(String realm, String abortedBy);

    /**
     *
     * @param realm the realm
     * @param lockUser the user who locks the realm
     * @return session uuid
     * @throws OptimisticLockException Realm all ready in session use.
     * @throws PessimisticLockException Could not lock the master entry (30
     * seconds timeout).
     */
    UUID createSession(String realm, String lockUser);

    /**
     * Release the session.
     *
     * @param session session uuid
     * @return 1 if session found
     */
    int releaseSession(UUID session);
    
    /**
     * Remove the session.
     *
     * @param session session uuid
     * @throws NoResultException if session not found
     */
    void removeSession(UUID session);
    
}
