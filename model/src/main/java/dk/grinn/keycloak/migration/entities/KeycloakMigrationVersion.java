package dk.grinn.keycloak.migration.entities;

/*-
 * #%L
 * Keycloak : Migrate : Spi
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@NamedQuery(
        name = "getAllMigrations",
        query = "from KeycloakMigrationVersion"
)
@Table(name = "keycloak_migration_version")
public class KeycloakMigrationVersion implements Serializable {

    @Id
    @Column(name = "version")
    private String version;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "script", nullable = false)
    private String script;

    @Column(name = "checksum", nullable = false)
    private String checksum;

    @Column(name = "installed_by")
    private String installedBy;

    @Column(name = "installed_on", nullable = false)
    private LocalDateTime installedOn;

    @Column(name = "execution_time", nullable = false)
    private Integer executionTime;

    @Column(name = "success", nullable = false)
    private Boolean success;

    @Column(name = "keycloak_migration_session_id", nullable = false)
    private Boolean keycloakMigrationSessionId;

    public String getVersion() {
        return version;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    public String getScript() {
        return script;
    }

    public String getChecksum() {
        return checksum;
    }

    public String getInstalledBy() {
        return installedBy;
    }

    public LocalDateTime getInstalledOn() {
        return installedOn;
    }

    public Integer getExecutionTime() {
        return executionTime;
    }

    public Boolean getSuccess() {
        return success;
    }

    public Boolean getKeycloakMigrationSessionId() {
        return keycloakMigrationSessionId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.version);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KeycloakMigrationVersion other = (KeycloakMigrationVersion) obj;
        return Objects.equals(this.version, other.version);
    }


}
