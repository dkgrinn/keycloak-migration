package dk.grinn.keycloak.migration.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Model
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.migration.entities.ScriptHistory;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
public interface ScriptResource {

    /**
     * Get a script entry.
     *
     * @param id scriptId
     * @param rank scriptRank
     * @return ScriptHistory
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/{rank}")
    ScriptHistory getScript(@PathParam("id") int id, @PathParam("rank") int rank);

    /**
     * Get a list of all scripts in ranked sort order (creation order).
     *
     * @return list of ScriptHistory
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<ScriptHistory> getScripts();

    /**
     * Register the beginning of a new script.
     *
     * @param args request args
     * @param repeatable is repeatable flag
     * @return post response
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    Response begin(ScriptBegin args, @QueryParam("repeatable") @DefaultValue("false") boolean repeatable);

    /**
     * (Re)open a previously commit'tet script.
     * 
     * @param version scriptVersion
     * @return  patch response
     */
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{version}")
    Response open(@PathParam("version") String version);
    
    /**
     * Register script end.
     *
     * @param id scriptId
     * @param rank scriptRank
     * @param args request args
     * @return put response
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}/{rank}")
    Response commit(@PathParam("id") int id, @PathParam("rank") int rank, ScriptCommit args);
    
    /**
     * Abort the current registration.
     *
     * @param id scriptId
     * @param rank scriptRank
     * @return delete response
     */
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}/{rank}")
    Response abort(@PathParam("id") int id, @PathParam("rank") int rank);
}
