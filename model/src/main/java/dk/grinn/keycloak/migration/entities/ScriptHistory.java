package dk.grinn.keycloak.migration.entities;

/*-
 * #%L
 * Keycloak : Migrate : Model
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
public class ScriptHistory {

    private String version;
    
    private String script;
    
    private Long checksum;
    
    private Integer executionTime;
    
    private Date installedOn;
    
    private String installBy;

    public ScriptHistory() {
    }

    public ScriptHistory(String version, String script, Long checksum, Integer executionTime, Date installedOn, String installBy) {
        this.version = version;
        this.script = script;
        this.checksum = checksum;
        this.executionTime = executionTime;
        this.installedOn = installedOn;
        this.installBy = installBy;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public Long getChecksum() {
        return checksum;
    }

    public void setChecksum(Long checksum) {
        this.checksum = checksum;
    }

    public Integer getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(Integer executionTime) {
        this.executionTime = executionTime;
    }

    public Date getInstalledOn() {
        return installedOn;
    }

    public void setInstalledOn(Date installedOn) {
        this.installedOn = installedOn;
    }

    public String getInstallBy() {
        return installBy;
    }

    public void setInstallBy(String installBy) {
        this.installBy = installBy;
    }

    @Override
    public String toString() {
        return String.format("Script: %s Version: %s, Checksum: %s, Intalled on: %s, Installed by: %s", script, version, (checksum == null ? "NULL" : checksum), installedOn, installBy);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.version);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ScriptHistory other = (ScriptHistory) obj;
        return Objects.equals(this.version, other.version);
    }
}
