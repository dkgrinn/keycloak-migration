package dk.grinn.keycloak.migration.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Model
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.migration.entities.Session;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
public interface RealmResource {
    
    /**
     * Abort an active session of a realm.
     * <p>
     * Only to be used in an abnormal situation where the session id has been
     * lost (caused by an error on a previous run, etc.).
     * 
     * @param realm the realm
     * @return deleted response
     */
    @DELETE
    @Path("{realm}")
    @Produces(MediaType.APPLICATION_JSON)
    Response abortSession(@PathParam("realm") String realm);

    /**
     * Get all scripts from all realms.
     *
     * @return all realm scripts
     */
    @GET
    @Path("scripts")
    @Produces(MediaType.APPLICATION_JSON)
    List<Session> getAllRealmScripts();
    
    /**
     * Access to scripts on a realm.
     * 
     * @param realm the realm
     * @return RealmScriptResource
     */
    @Path("{realm}/scripts")
    RealmScriptResource scripts(@PathParam("realm") String realm);

}
