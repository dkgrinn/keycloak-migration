package dk.grinn.keycloak.migration.entities;

import static java.lang.Integer.parseInt;
import java.nio.file.Path;

/*-
 * #%L
 * Keycloak : Migrate : Model
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

/**
 *
 * @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
public class ScriptHistoryKey {

    private int id;
    
    private int rank;

    public ScriptHistoryKey() {
    }

    public ScriptHistoryKey(int id, int rank) {
        this.id = id;
        this.rank = rank;
    }

    public ScriptHistoryKey(String id, String rank) {
        this(parseInt(id), parseInt(rank));
    }
    
    public ScriptHistoryKey(Path id, Path rank) {
        this(id.toString(), rank.toString());
    }
    
    public ScriptHistoryKey(Path location) {
        this(location.getParent().getFileName(), location.getFileName());
        
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + this.id;
        hash = 37 * hash + this.rank;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ScriptHistoryKey other = (ScriptHistoryKey) obj;
        if (this.id != other.id) {
            return false;
        }
        return this.rank == other.rank;
    }

    @Override
    public String toString() {
        return "ScriptHistoryKey{" + "id=" + id + ", rank=" + rank + '}';
    }
    
}
