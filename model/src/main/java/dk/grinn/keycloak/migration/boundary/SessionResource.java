package dk.grinn.keycloak.migration.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Model
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */
import dk.grinn.keycloak.migration.entities.CreateRealmKey;
import dk.grinn.keycloak.migration.entities.Session;

import java.util.List;
import java.util.UUID;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
public interface SessionResource {

    /**
     * Get sessions.
     * <p>
     * This is all realms which have been created using the migration tool.
     *
     * @param activeOnly If set to true only returns active sessions.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<Session> getSessions(
            @QueryParam("activeOnly") @DefaultValue("false") boolean activeOnly
    );

    /**
     * Get an active session.
     *
     */
    @GET
    @Path("{sessionId}")
    @Produces(MediaType.APPLICATION_JSON)
    Session getSession(@PathParam("sessionId") UUID sessionId);

    /**
     *
     */
    @Path("{sessionId}/scripts")
    ScriptResource scripts(@PathParam("sessionId") UUID sessionId);

    @POST
    @Path("{sessionId}/realm")
    @Consumes(MediaType.APPLICATION_JSON)
    Response setRealmKey(@PathParam("sessionId") UUID sessionId, CreateRealmKey realm);


    @PUT
    @Path("{sessionId}/realm/disableRsaGeneratedKey")
    @Consumes(MediaType.APPLICATION_JSON)
    Response disableRsaGeneratedKey(@PathParam("sessionId") UUID sessionId, String realmId);

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response createSession(CreateSession args);

    /**
     * Release an active session.
     * <p>
     * After a successful call the <code>sessionId</code> is no longer valid.
     *
     */
    @PUT
    @Path("{sessionId}")
    @Produces(MediaType.APPLICATION_JSON)
    Response releaseSession(@PathParam("sessionId") UUID sessionId);

    /**
     * Removes an active session.
     * <p>
     * This completely removes the DB history lock (session) entry and all it's
     * associated history entries (scripts).
     *
     */
    @DELETE
    @Path("{sessionId}")
    @Produces(MediaType.APPLICATION_JSON)
    Response removeSession(@PathParam("sessionId") UUID sessionId);
}
