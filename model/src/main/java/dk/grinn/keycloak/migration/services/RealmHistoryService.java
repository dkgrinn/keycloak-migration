package dk.grinn.keycloak.migration.services;

/*-
 * #%L
 * Keycloak : Migrate : Model
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.migration.entities.ScriptHistory;
import dk.grinn.keycloak.migration.entities.ScriptHistoryKey;
import java.util.List;
import java.util.UUID;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

/**
 *
 * @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
public interface RealmHistoryService {

    /**
     * Get the script history.
     *
     * @param session session uuid
     * @return Script history. Ranked (ordered) by the initial creation order.
     * @throws NoResultException No history on the session or session not found.
     */
    List<ScriptHistory> getHistory(UUID session);

    /**
     * Get a script history entry.
     *
     * @param session session uuid
     * @param id the id
     * @param installedRank the rank
     * @return Script history. Ranked (ordered) by the initial creation order.
     * @throws NoResultException No history on the session or session not found.
     */
    ScriptHistory getHistory(UUID session, int id, int installedRank);

    /**
     * Get a script history entry.
     * 
     * @param realm the realm
     * @param version the version
     * @return  ScriptHistory
     */
    ScriptHistory getHistory(String realm, String version);
    
    /**
     * Get the script history of the realm.
     *
     * @param realm the realm
     * @return Script history. Ranked (ordered) by initial creation order.
     */
    List<ScriptHistory> getHistory(String realm);

    /**
     * Register begin of the script.
     *
     * @param session session uuid
     * @param version script version
     * @param description script description
     * @param type script type
     * @param installedBy name of user who installed script
     * @return ScriptHistoryKey
     * @throws NoResultException Not a valid session.
     * @throws NonUniqueResultException A script is all ready being processed
     */
    default ScriptHistoryKey scriptBegin(UUID session, String version, String description, String type, String installedBy) {
        return scriptBegin(session, version, description, type, installedBy, false);
    }
    
    /**
     * Register begin of the script.
     *
     * @param session session uuid
     * @param version script version
     * @param description script description
     * @param type script type
     * @param installedBy name of user who installed script
     * @param repeatable is repeatable boolean flag
     * @return ScriptHistoryKey
     * @throws NoResultException Not a valid session.
     * @throws NonUniqueResultException A script is all ready being processed
     */
    ScriptHistoryKey scriptBegin(UUID session, String version, String description, String type, String installedBy, boolean repeatable);

    /**
     * (Re)open an existing script.
     *
     * @param session session uuid
     * @param version script version
     * @param openedBy name of user who opened script
     * @return ScriptHistoryKey
     * @throws NoResultException Not a valid session or the script does not exists.
     * @throws NonUniqueResultException A script is all ready being processed
     */
    ScriptHistoryKey scriptOpen(UUID session, String version, String openedBy);
    
    /**
     * Commit (close) the history script entry.
     * <p>
     * The history entry is updated with the information collected by the client
     * responsible of the migration.
     *
     * @param session session uuid
     * @param key As obtained by {@link #scriptBegin}
     * @param script (Not null) Typically the name of the script.
     * @param checksum Checksum
     * @param executionTime In milliseconds - how long time the migration took.
     * @throws NoResultException Unable to find the open history script entry.
     */
    void scriptCommit(UUID session, ScriptHistoryKey key, String script, Long checksum, int executionTime);
    
    /**
     * Remove the history script entry.
     *
     * @param session session uuid
     * @param key As obtained by {@link #scriptBegin}
     * @throws NoResultException Unable to find the open history script entry.
     */
    void scriptRemove(UUID session, ScriptHistoryKey key);
    
}
