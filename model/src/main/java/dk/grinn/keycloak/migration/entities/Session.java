package dk.grinn.keycloak.migration.entities;

/*-
 * #%L
 * Keycloak : Migrate : Model
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import java.nio.ByteBuffer;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
public class Session {

    private String realm;

    private UUID sessionId;

    private Date lockGranted;

    private String lockedBy;
    
    private List<ScriptHistory> scripts;

    public Session() {
    }
    
    public Session(String realm, byte[] session, Date lockGranted, String lockedBy) {
        this.realm = realm;
        this.lockGranted = lockGranted;
        this.lockedBy = lockedBy;
        if (session != null) {
            ByteBuffer bb = ByteBuffer.wrap(session);
            this.sessionId = new UUID(bb.getLong(), bb.getLong());
        }
    }
    
    public String getRealm() {
        return realm;
    }

    public void setRealm(String realm) {
        this.realm = realm;
    }

    public UUID getSession() {
        return sessionId;
    }

    public void setSession(UUID sessionId) {
        this.sessionId = sessionId;
    }

    public Date getLockGranted() {
        return lockGranted;
    }

    public void setLockGranted(Date lockGranted) {
        this.lockGranted = lockGranted;
    }

    public String getLockedBy() {
        return lockedBy;
    }

    public void setLockedBy(String lockedBy) {
        this.lockedBy = lockedBy;
    }

    public List<ScriptHistory> getScripts() {
        return scripts;
    }

    public void setScripts(List<ScriptHistory> scripts) {
        this.scripts = scripts;
    }

}
