# Keycloak migration

## Developement

Allows for local development and deployment of Keycloak components on a locally
installed Keycloak server - automatically installed by the development tools.  
Server installation and configuration are all maven handled.

**OS NOTE** Runs equally well on Linux, Mac and Windows.  
Simply requires open-jdk11 (or newer) and maven installed and available in your path.  
The command line examples are all Linux/Mac.  
For Windows substitute `./keycloak` with `keycloak`

### First time config and server installation

  * `./keycloak package`  
    Press &lt;enter&gt; when prompted
  * Edit the file `build.properties`  
    The default settings should be adequate for normal development using the buit-in H2 database.
  * Rerun the installation: 
    `./keycloak package`

### (Optional) Using external database - mariadb

Keycloak can be set up using an external database.  
Instructions shown here are for MariaDB but it can easily be adapted for other types of RDBM's. 

  * Create a new schema and db user with sql commands from `ear/src/main/sql/00-create-db.sql`  
```sql
create user if not exists 'keycloak'@'%' identified by '<password>';
create database keycloak_fut character set utf8 collate utf8_unicode_ci;
grant all privileges on keycloak_fut.* to 'keycloak'@'%';
flush privileges;
```
  * Update the file `build.properties` adjusting the properties:  
    `keycloak.db.url`, `keycloak.db.username` and `keycloak.db.password` accordingly
  * To install the jdbc driver and configure the keycloak datasource - run:  
    `./keycloak mariadb`

## Running the Keycloak server

The root folder contains a small java 11 single source script which
uses the `build.properties` configuration to locate the installation, use
the specified port offset and use the specified configuration.  
To run the server:  
`./keycloak run`

Any arguments given to the `keycloak` script are passed directly on to the Keycloak
`standalone.(sh|bat)` script.  
As an example is to run the server in debug mode:  
`./keycloak run --debug`

### Using and IDE
Use the application server integration of the IDE (Intellij, Netbeans, etc.).
  * Just point the IDE appserver configuration to `${server.sites}/keycloak-6.0.1`
  * Adjust the port settings to match the `keycloak.port.offset` setting.  
    Intellij supports port offsets directly. The other IDE's requires the port
    to be specified directly - ie. management port at 9994 for port offset 4.
    
And you should be good for starting/stopping and running the EAR file and modules
as part of the normal development cycle.


### Some convenience developer commands

For usage info run:  
`./keycloak`

Compile, (re)install the wildfly module (spi and model parts) and run the appserver:  
`./keycloak module run`

Compile and install the EAR application  
`mvn -Plocalhost install`  
**NOTE** Keycloak needs to be running - so do this from another prompt or from within your IDE.

Developing Liquibase db migrations on the internal H2 database:  
`./keycloak clean-all package module run`

### JPA development (spi module)

The developer environment has been setup to allow for test driven development using
Liquibase maven plugin (changelog db changes) and JUnit 5 Extension to inject the
Hibernate EntityManager into controllers.

The following uses mariadb as example. But any database supported by Liquibase and
Hibernate should do the trick.

**First time setup**

  * Edit `build.properties`  
    Properties to be edited: `liquibase.url`, `liquibase.driver`, `liquibase.username`, `liquibase.password` 
  * Create the db schema used in `liquibase.url`  
    Ie. if `liquibase.url` is `jdbc:mariadb://localhost:3306/liquibase_keycloak_migrate`  
    then use `create schema liquibase_keycloak_migrate;` from your favorite mysql/mariadb environment.

**Liquibase maven command**

`mvn -Plocalhost --projects spi initialize liquibase:dropAll liquibase:update`

**Test driven development**

Hibernate setup is done in `spi/src/test/java/dk/grinn/keycloak/migration/test/EntityManagerExtension.java`  
It uses the Liquibase information defined in `build.properties`. So it should run-out-of-the-box.

Examples - see the integration tests in: `spi/src/test/java/dk/grinn/keycloak/migration/boundary/*`

#### Reinstall and reconfigure Keycloak from scratch

`./keycloak clean package module`

Using mariadb configuration  
`./keycloak clean mariadb module`

It's actually possible to do a full circle in one go:  
`./keycloak clean package module run`

Useful when developing cli scripts and other configuration stuff where the keycloak
configuration might get tangled up!

**NOTE**: The `${jboss.home}/standalone/data/**/*` directory elements are **not** delete by `clean`.  
For normally behaving applications this means that queue elements, timers, 
batch processing, H2 database, etc. will survive the reinstallation.  
A complete wipe requires the `clean-all` argument instead of `clean`.

## Production deployment package

A distribution package (.zip and .tar.gz) will be release. The package will include
two jboss-cli scripts for installation (one for each of the two running modes -
standalone and domain).

## Docker image

Some work have been done, but nothing production ready.

The image will be compatible with official Keycloak docker image in regards of environment
settings - db settings, etc.