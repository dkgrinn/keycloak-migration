@echo off
REM
REM Java 11 script launcher
REM
REM @author Kim Jersin (kij@trifork.com)
REM
setlocal

REM Resolve absolute path to directory of our command script
set "DIRNAME=%~dp0%"
pushd "%DIRNAME%"
set "BINDIR=%CD%"
popd

REM Full path of our script
REM Same base name as the .cmd file
set "JS=%BINDIR%\%~n0"

REM To pause or not to pause?
REM (double click in explorer will keep the console window open on script end)?
set interactive=1
echo %cmdcmdline% | find /i "%~0" >nul
if not errorlevel 1 set interactive=0

REM Check for java command
where /q java
if errorlevel 1 (
    echo FATAL: java.exe command not found
    echo Please add the directory %%JAVA_HOME%%\bin to the command line path of the windows environment
    if _%interactive%_==_0_ pause
    exit /B
)

REM Run the script and pass on all arguments
java --source 11 %JS% %*
if _%interactive%_==_0_ pause
