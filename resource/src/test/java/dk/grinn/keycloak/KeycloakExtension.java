package dk.grinn.keycloak;

/*-
 * #%L
 * Keycloak : Migrate : Resource
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import static java.nio.file.Files.newInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.UriBuilder;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;

/**
 *
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
public class KeycloakExtension implements BeforeAllCallback, AfterAllCallback, ParameterResolver {

    private final Properties config = new Properties();

    private Keycloak keycloak;

    public KeycloakExtension() {
        this("../build.properties");
    }

    public KeycloakExtension(String configFile) {
        this(Paths.get(configFile));
    }

    public KeycloakExtension(Path configFile) {
        try (InputStream is = newInputStream(configFile)) {
            config.load(is);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void beforeAll(ExtensionContext arg0) {
        // Build the connection
        String gkcadmUrl = config.getProperty("gkcadm.url");
        keycloak = KeycloakBuilder.builder()
                .serverUrl(gkcadmUrl)
                .realm("master")
                .username(config.getProperty("gkcadm.admin.username"))
                .password(config.getProperty("gkcadm.admin.password"))
                .clientId("admin-cli")
                .build();

        // Check the connection
        keycloak.serverInfo().getInfo();
    }

    @Override
    public void afterAll(ExtensionContext arg0) {
        keycloak.close();
    }

    @Override
    public boolean supportsParameter(ParameterContext pc, ExtensionContext ec) throws ParameterResolutionException {
        Class<?> type = pc.getParameter().getType();
        return type.equals(Keycloak.class)
                || type.equals(UriBuilder.class)
                || type.equals(Properties.class)
                || pc.isAnnotated(PathParam.class);
    }

    @Override
    public Object resolveParameter(ParameterContext pc, ExtensionContext ec) throws ParameterResolutionException {
        Class<?> type = pc.getParameter().getType();
        if (type.equals(Keycloak.class)) {
            return keycloak;
        }
        String gkcadmUrlPath = "realms/master/gkcadm";
        if (type.equals(UriBuilder.class)) {
            return UriBuilder.fromUri(config.getProperty("gkcadm.url") + "/" + gkcadmUrlPath);
        }
        if (type.equals(Properties.class)) {
            return config;
        }
        PathParam resourcePath = pc.getParameter().getAnnotation(PathParam.class);
        if (resourcePath != null) {
            try {
                return keycloak.proxy(type, new URI(config.getProperty("gkcadm.url") + "/" + gkcadmUrlPath + "/" + resourcePath.value()));
            } catch (URISyntaxException ex) {
                throw new ParameterResolutionException(ex.getMessage(), ex);
            }
        }
        return null;
    }
}
