package dk.grinn.keycloak.migration.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Resource
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.KeycloakExtension;
import dk.grinn.keycloak.migration.entities.Session;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import static javax.servlet.http.HttpServletResponse.SC_CREATED;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;
import static javax.servlet.http.HttpServletResponse.SC_NO_CONTENT;
import static org.apache.commons.lang.RandomStringUtils.randomAlphabetic;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
@ExtendWith(KeycloakExtension.class)
class SessionRessourceImplIT {

    private String realm;

    private static SessionResource resource;

    @BeforeAll
    static void resource(@PathParam("migrations/sessions") SessionResource proxyedResource) {
        resource = proxyedResource;
    }

    @BeforeEach
    void randomRealm() {
        this.realm = randomAlphabetic(10);
    }

    /**
     * Test of getSessions method, of class SessionRest.
     */
    @Test
    void testGetSessions() {
        // Master should be present as non active
        List<Session> sessions = resource.getSessions(false);
        assertTrue(sessions.stream().anyMatch(
                (session) -> session.getRealm().equals("master")
        ));

        // Master should be present as non active only
        sessions = resource.getSessions(true);
        assertFalse(sessions.stream().anyMatch(
                (session) -> session.getRealm().equals("master")
        ));
    }

    @Test
    void testCreateAndGetSession() {
        Response response = resource.createSession(new CreateSession(realm));
        assertEquals(SC_CREATED, response.getStatus());
        UUID sessionId = UUID.fromString(Paths.get(response.getLocation().getPath()).getFileName().toString());

        Session session = resource.getSession(sessionId);
        assertEquals(realm, session.getRealm());
        assertEquals(sessionId, session.getSession());
        assertEquals(SC_NO_CONTENT, resource.releaseSession(sessionId).getStatus());
    }

    @Test
    void testReleaseSession() {
        Response response = resource.createSession(new CreateSession(realm));
        assertEquals(SC_CREATED, response.getStatus());
        UUID sessionId = UUID.fromString(Paths.get(response.getLocation().getPath()).getFileName().toString());

        assertEquals(SC_NO_CONTENT, resource.releaseSession(sessionId).getStatus());
        assertEquals(SC_NOT_FOUND, resource.releaseSession(sessionId).getStatus());

        // Reopen for different user
        response = resource.createSession(new CreateSession(realm));
        assertEquals(SC_CREATED, response.getStatus());
        UUID sessionId2 = UUID.fromString(Paths.get(response.getLocation().getPath()).getFileName().toString());
        assertNotEquals(sessionId, sessionId2);
        assertEquals(SC_NO_CONTENT, resource.releaseSession(sessionId2).getStatus());
    }

    @Test
    void testAbortSession(@PathParam("migrations/realms") RealmResource realmResource) {
        Response response = resource.createSession(new CreateSession(realm));
        assertEquals(SC_CREATED, response.getStatus());

        assertEquals(SC_NO_CONTENT, realmResource.abortSession(realm).getStatus());
        assertEquals(SC_NOT_FOUND, realmResource.abortSession(realm).getStatus());
    }
}
