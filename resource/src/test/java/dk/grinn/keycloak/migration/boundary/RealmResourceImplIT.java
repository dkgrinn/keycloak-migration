package dk.grinn.keycloak.migration.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Resource
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.KeycloakExtension;
import dk.grinn.keycloak.migration.entities.Session;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.admin.client.Keycloak;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.UriBuilder;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
@ExtendWith(KeycloakExtension.class)
public class RealmResourceImplIT {

    private static RealmResource resource;

    @BeforeAll
    public static void createSession(
            Keycloak keycloak, UriBuilder sessionUriBuilder, UriBuilder uriBuilder,
            @PathParam("migrations/sessions") SessionResource sessionResource
    ) throws MalformedURLException, URISyntaxException {
        ScriptResourceImplIT.createSession(keycloak, sessionUriBuilder, sessionResource);

        // The resource to test
        resource = keycloak.proxy(RealmResource.class, uriBuilder
                .path("migrations").path("realms")
                .build().toURL().toURI()
        );

    }

    @AfterAll
    public static void releaseSession(@PathParam("migrations/sessions") SessionResource sessionResource) {
        ScriptResourceImplIT.releaseSession(sessionResource);
    }

    /**
     * Test of getAllRealmScripts method, of class RealmResourceImpl.
     */
    @Test
    void testGetAllRealmScripts() {
        ScriptResourceImplIT scriptResource = new ScriptResourceImplIT();
        scriptResource.testGetScripts();

        List<Session> scripts = resource.getAllRealmScripts();
        assertTrue(scripts.size() > 1);
    }

}
