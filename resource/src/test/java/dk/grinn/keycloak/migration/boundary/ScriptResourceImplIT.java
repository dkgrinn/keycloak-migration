package dk.grinn.keycloak.migration.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Resource
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.KeycloakExtension;
import dk.grinn.keycloak.migration.entities.ScriptHistory;
import dk.grinn.keycloak.migration.entities.ScriptHistoryKey;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.admin.client.Keycloak;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.CRC32;

import static java.util.Comparator.comparing;
import static javax.servlet.http.HttpServletResponse.SC_CREATED;
import static javax.servlet.http.HttpServletResponse.SC_NO_CONTENT;
import static org.apache.commons.lang.RandomStringUtils.randomAlphabetic;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
@ExtendWith(KeycloakExtension.class)
class ScriptResourceImplIT {

    private static UUID sessionId;

    private static ScriptResource resource;

    static final AtomicInteger version = new AtomicInteger();

    static String realm;

    @BeforeAll
    static void createSession(
            Keycloak keycloak, UriBuilder uriBuilder,
            @PathParam("migrations/sessions") SessionResource sessionResource
    ) throws MalformedURLException, URISyntaxException {
        // Need a realm and a migration createSession
        realm = randomAlphabetic(10);
        Response response = sessionResource.createSession(new CreateSession(realm));
        assertEquals(SC_CREATED, response.getStatus());
        sessionId = UUID.fromString(
                Paths.get(response.getLocation().getPath()).getFileName().toString()
        );

        // The resource to test
        resource = keycloak.proxy(ScriptResource.class, uriBuilder
                .path("migrations").path("sessions")
                .path(sessionId.toString()).path("scripts")
                .build().toURL().toURI()
        );
    }

    @AfterAll
    static void releaseSession(@PathParam("migrations/sessions") SessionResource sessionResource) {
        sessionResource.releaseSession(sessionId);
    }

    @Test
    void testGetScript(Properties config) {
        ScriptBegin nextScript = nextScript();
        ScriptHistoryKey key = getKey(resource.begin(nextScript, false));
        commit(nextScript, key);
        ScriptHistory script = resource.getScript(key.getId(), key.getRank());

        assertEquals(nextScript.getVersion(), script.getVersion());
        assertNotNull(script.getChecksum());
        assertEquals(config.getProperty("gkcadm.admin.username"), script.getInstallBy());
        assertTrue(script.getExecutionTime() > 400);
    }

    /**
     * Test of getScripts method, of class ScriptResourceImpl.
     */
    @Test
    void testGetScripts() {
        // Create some scripts to test apon
        for (int i = 0; i < 10; i++) {
            testBeginAndCommit();
        }

        List<ScriptHistory> scripts = resource.getScripts();
        assertTrue(scripts.size() >= 10);

        // Check ranking (sort order)
        ArrayList<ScriptHistory> copy = new ArrayList<>(scripts);
        copy.sort(comparing(ScriptHistory::getVersion));
        assertEquals(scripts, copy);
    }

    /**
     * Test of begin method, of class ScriptResourceImpl.
     */
    @Test
    void testBeginAndCommit() {
        ScriptBegin script = nextScript();
        ScriptHistoryKey key = getKey(resource.begin(script, false));
        commit(script, key);
    }

    @Test
    void testOpen() {
        ScriptBegin script = nextScript();
        ScriptHistoryKey key = getKey(resource.begin(script, false));
        commit(script, key);

        ScriptHistoryKey key2 = getKey(resource.open(script.getVersion()));
        assertEquals(key, key2);
        commit(script, key);
    }

    @Test
    void testAbort() {
        ScriptBegin script = nextScript();
        ScriptHistoryKey key = getKey(resource.begin(script, false));
        resource.abort(key.getId(), key.getRank());
    }

    private ScriptHistoryKey getKey(Response response) {
        assertEquals(SC_CREATED, response.getStatus());
        return new ScriptHistoryKey(Paths.get(response.getLocation().getPath()));
    }

    private ScriptBegin nextScript() {
        int nextVersion = version.incrementAndGet();
        return new ScriptBegin(
                String.format("V%02d", nextVersion),
                String.format("V%02d - description", nextVersion)
        );
    }

    private void commit(ScriptBegin script, ScriptHistoryKey key) {
        CRC32 crc = new CRC32();
        crc.update(script.getVersion().getBytes());
        Response response = resource.commit(key.getId(), key.getRank(), new ScriptCommit(
                String.format("%s__filename_for_%s", script.getVersion(), script.getVersion()),
                crc.getValue(), 400 + version.get())
        );
        assertEquals(SC_NO_CONTENT, response.getStatus());
    }
}
