package dk.grinn.keycloak.admin.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Resource
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.migration.entities.ServerInfo;
import org.eclipse.microprofile.health.HealthCheck;

import javax.annotation.security.RolesAllowed;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
@RolesAllowed("admin")
public class InfoResourceImpl implements InfoResource {

    private final List<HealthCheck> healths;

    public InfoResourceImpl(List<HealthCheck> healths) {
        this.healths = healths;
    }

    @Override
    public List<ServerInfo> info() {
        return healths.stream()
                .map(HealthCheck::call)
                .filter(r -> r.getData().isPresent() && r.getData().get().containsKey("version"))
                .map(r -> new ServerInfo(r.getName(), r.getData().get().get("version").toString()))
                .collect(toList());
    }
}
