package dk.grinn.keycloak.admin.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Spi
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import org.keycloak.models.KeycloakSession;
import org.keycloak.services.managers.AppAuthManager;
import org.keycloak.services.resource.RealmResourceProvider;

import javax.naming.NamingException;
import java.util.List;

import static dk.grinn.keycloak.migration.resource.RealmAccessAuthFilter.requireAuthentication;
import static java.lang.String.format;
import static java.lang.System.getProperty;
import static javax.naming.InitialContext.doLookup;

public class GkcAdmResourceProvider implements RealmResourceProvider {

    private static final String RESOURCE_JNDI_NAME = format(
            "java:global/%s/keycloak-migrate-resource/GkcAdmResourceImpl",
            getProperty("keycloak.migration.application-name")
    );

    private final KeycloakSession session;

    private final AppAuthManager appAuthManager;

    private final List<String> validRealms;

    public GkcAdmResourceProvider(KeycloakSession session, AppAuthManager appAuthManager, List<String> validRealms) {
        this.session = session;
        this.appAuthManager = appAuthManager;
        this.validRealms = validRealms;
    }

    /**
     * Returns the root JAX-RS resource instance of the gkc admin module.
     *
     * @return a JAX-RS sub-resource instance
     */
    @Override
    public Object getResource() {
        try {
            // Signal authentication required
            requireAuthentication(session, appAuthManager, validRealms);

            // 
            GkcAdmResourceImpl resource = doLookup(RESOURCE_JNDI_NAME);
            resource.setSession(session);
            return resource;
        } catch (NamingException ex) {
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public void close() {
        // nothing to do
    }

}
