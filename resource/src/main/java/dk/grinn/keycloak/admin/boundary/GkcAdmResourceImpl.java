package dk.grinn.keycloak.admin.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Spi
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.admin.ServerInfoRegistry;
import dk.grinn.keycloak.migration.boundary.MigrationResource;
import dk.grinn.keycloak.migration.boundary.MigrationResourceImpl;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import org.keycloak.models.KeycloakSession;

/**
 * Root JAX_RS resource for all bundled gkc admin functionality (currently this
 * is only migration - but more to come). 
 * 
 * <p>
 * <b>URL</b>: <pre>{@code /auth/realms/<realm>/gkcadm}</pre>
 * 
 * @see MigrationResourceImpl
 */
@LocalBean
@Stateful(passivationCapable = false)
public class GkcAdmResourceImpl implements GkcAdmResource {

    private KeycloakSession session;
    
    @Resource(lookup = "java:global/migration/server-info")
    protected ServerInfoRegistry health;
    
    public void setSession(KeycloakSession session) {
        this.session = session;
    }
    
    /**
     * Returns the migration JAX-RS resource instance.
     * 
     * @return a JAX-RS sub-resource instance
     */
    @Remove
    @Override
    public MigrationResource migrations() {
        return new MigrationResourceImpl(session);
    }

    @Remove
    @Override
    public InfoResource infoResource() {
        return new InfoResourceImpl(health.getAll());
    }
}
