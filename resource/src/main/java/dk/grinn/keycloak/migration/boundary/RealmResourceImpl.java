package dk.grinn.keycloak.migration.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Resource
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.migration.entities.Session;
import org.keycloak.connections.jpa.JpaConnectionProvider;
import org.keycloak.models.KeycloakSession;

import javax.annotation.security.RolesAllowed;
import javax.persistence.EntityManager;
import javax.ws.rs.core.Response;
import java.util.List;

import static dk.grinn.keycloak.migration.resource.RealmAccessAuthFilter.getAuthResult;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;

/**
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
@RolesAllowed("admin")
public class RealmResourceImpl implements RealmResource {

    private final KeycloakSession session;

    private final RealmHistoryController history;

    private final RealmHistoryLockController locks;

    public RealmResourceImpl(KeycloakSession session) {
        EntityManager em = session.getProvider(JpaConnectionProvider.class).getEntityManager();
        this.locks = new RealmHistoryLockController(em);
        this.history = new RealmHistoryController(em);
        this.session = session;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response abortSession(String realm) {
        return locks.abortSession(realm, getAuthResult(session).getToken().getPreferredUsername()) == 1
                ? Response.noContent().build()
                : Response.status(SC_NOT_FOUND).build();
    }

    @Override
    public List<Session> getAllRealmScripts() {
        final List<Session> sessions = locks.getSessions(false);
        sessions.forEach(s -> s.setScripts(history.getHistory(s.getRealm())));
        return sessions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RealmScriptResource scripts(String realm) {
        return new RealmScriptResourceImpl(history, realm);
    }

}
