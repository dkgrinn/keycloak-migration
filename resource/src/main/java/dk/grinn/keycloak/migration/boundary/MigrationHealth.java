package dk.grinn.keycloak.migration.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Resource
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.admin.ServerInfoRegistry;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;
import org.eclipse.microprofile.health.Liveness;
import org.eclipse.microprofile.health.Readiness;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.Properties;

import static org.eclipse.microprofile.health.HealthCheckResponse.builder;

/**
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
@Liveness
@Readiness
@ApplicationScoped
public class MigrationHealth implements HealthCheck {

    @Resource(lookup = "java:global/migration/server-info")
    protected ServerInfoRegistry info;

    @Override
    public HealthCheckResponse call() {
        HealthCheckResponseBuilder response = builder().name("migration");

        // Locate our maven pom version
        try (InputStream is = MigrationHealth.class.getResourceAsStream("/META-INF/maven/dk.grinn.keycloak/keycloak-migrate-resource/pom.properties")) {
            // Load the pom properties
            Properties pom = new Properties();
            pom.load(is);

            // Set as info data
            pom.forEach((key, value) -> response.withData(key.toString(), value.toString()));
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }

        return response.up().build();
    }

    @PostConstruct
    public void registrer() {
        info.register(this);
    }

    @PreDestroy
    public void unRegister() {
        info.unRegister(this);
    }
}
