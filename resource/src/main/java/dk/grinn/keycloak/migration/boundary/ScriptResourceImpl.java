package dk.grinn.keycloak.migration.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Resource
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */
import dk.grinn.keycloak.migration.entities.ScriptHistory;
import dk.grinn.keycloak.migration.entities.ScriptHistoryKey;
import static dk.grinn.keycloak.migration.resource.RealmAccessAuthFilter.getAuthResult;
import java.util.List;
import java.util.UUID;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.core.Response;
import org.keycloak.connections.jpa.JpaConnectionProvider;
import org.keycloak.models.KeycloakSession;

/**
 *
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
@RolesAllowed("admin")
public class ScriptResourceImpl implements ScriptResource {

    private final UUID sessionId;

    private final KeycloakSession session;

    private final RealmHistoryController controller;

    public ScriptResourceImpl(KeycloakSession session, UUID sessionId) {
        this.controller = new RealmHistoryController(session.getProvider(JpaConnectionProvider.class).getEntityManager());
        this.sessionId = sessionId;
        this.session = session;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ScriptHistory getScript(int id, int rank) {
        return controller.getHistory(sessionId, id, rank);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ScriptHistory> getScripts() {
        return controller.getHistory(sessionId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response begin(ScriptBegin args, boolean repeatable) {
        ScriptHistoryKey key = controller.scriptBegin(sessionId,
                args.getVersion(), args.getDescription(), args.getType(),
                getAuthResult(session).getToken().getPreferredUsername(),
                repeatable
        );
        return Response.created(
                session.getContext().getUri().getAbsolutePathBuilder()
                        .path(Integer.toString(key.getId()))
                        .path(Integer.toString(key.getRank()))
                        .build()
        ).build();
    }

    /**
     * {@inheritDoc
     */
    @Override
    public Response commit(int id, int rank, ScriptCommit args) {
        controller.scriptCommit(sessionId, new ScriptHistoryKey(id, rank),
                args.getScript(), args.getChecksum(), args.getExecutionTime()
        );
        return Response.noContent().build();
    }

    /**
     * {@inheritDoc
     */
    @Override
    public Response abort(int id, int rank) {
        controller.scriptRemove(sessionId, new ScriptHistoryKey(id, rank));
        return Response.noContent().build();
    }

    /**
     * {@inheritDoc
     */
    @Override
    public Response open(String version) {
        ScriptHistoryKey key = controller.scriptOpen(
                sessionId, version,
                getAuthResult(session).getToken().getPreferredUsername()
        );
        return Response.created(
                session.getContext().getUri().getAbsolutePathBuilder()
                        .path(Integer.toString(key.getId()))
                        .path(Integer.toString(key.getRank()))
                        .build()
        ).build();
    }

}
