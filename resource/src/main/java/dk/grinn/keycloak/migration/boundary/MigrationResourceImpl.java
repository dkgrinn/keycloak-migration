package dk.grinn.keycloak.migration.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Spi
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import org.jboss.resteasy.annotations.cache.NoCache;
import org.keycloak.models.KeycloakSession;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * JAX_RS resource for all migration functionality.
 * <p>
 * <b>URL</b>:
 * <pre>{@code /auth/realms/<realm>/gkcadm/migrations}</pre>
 *
 * @see MigrationResourceImpl
 */
@RolesAllowed({"admin"})
public class MigrationResourceImpl implements MigrationResource {

    private final KeycloakSession session;

    @SuppressWarnings("unused")
    public MigrationResourceImpl() {
        throw new UnsupportedOperationException("Default constructor not allowed");
    }

    public MigrationResourceImpl(KeycloakSession session) {
        this.session = session;
    }

    /**
     * Create, release, abort and list sessions.
     *
     */
    @Override
    public SessionResource sessions() {
        return new SessionRessourceImpl(session);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RealmResource realms() {
        return new RealmResourceImpl(session);
    }

    @POST
    @Path("")
    @NoCache
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public Response runMigrations() {
        return null;
    }

}
