package dk.grinn.keycloak.migration.cdi;

/*-
 * #%L
 * Keycloak : Migrate : CDI
 * %%
 * Copyright (C) 2019 - 2020 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */
import java.lang.management.ManagementFactory;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.management.ObjectName;
import org.apache.commons.configuration2.CompositeConfiguration;

/**
 *
 * @author Kim Jersin <kij@trifork.dk>
 */
@Startup
@Singleton
public class MigrationFactory {

    @Inject
    protected CompositeConfiguration config;

    @Inject
    protected KeycloakMigrate migrate;

    @PostConstruct
    public void init() {
        try {
            var server = ManagementFactory.getPlatformMBeanServer();
            var stateName = ObjectName.getInstance("jboss.root:type=state");
            var stateInfo = server.getMBeanInfo(stateName);

            System.out.println("Locations: " + config.getString("gkcadm.locations"));
            System.out.println("Commands:  " + config.getString("gkcadm.command.line.args"));

            var commands = config.getString("gkcadm.command.line.args", "info").trim().split(" ");
            for (var location : config.getList(String.class, "gkcadm.locations")) {
                execute(location, commands);
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public void execute(String location, String[] commands) throws Exception {
        for (String cmd : commands) {
            switch (cmd) {
                case "info":
                    migrate.info(location).get();
                    break;
                case "migrate":
                    migrate.migrate(location).get();
                    break;
                case "clean":
                    migrate.clean(location).get();
                    break;
                default:
                    System.out.println("Unexpected value: " + cmd);
            }
        }
    }
}
