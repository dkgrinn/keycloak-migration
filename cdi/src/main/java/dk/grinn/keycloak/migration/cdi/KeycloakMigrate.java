package dk.grinn.keycloak.migration.cdi;

/*-
 * #%L
 * Keycloak : Migrate : CDI
 * %%
 * Copyright (C) 2019 - 2020 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.admin.GkcadmConfiguration;
import dk.grinn.keycloak.admin.ProgressStream;
import dk.grinn.keycloak.migration.boundary.CreateSession;
import dk.grinn.keycloak.migration.boundary.SessionResource;
import dk.grinn.keycloak.migration.core.ClientMigrationContext;
import dk.grinn.keycloak.migration.core.migrate.MigratorService;
import dk.grinn.keycloak.migration.core.rest.Create;
import dk.grinn.keycloak.migration.entities.ScriptHistory;
import java.util.ArrayList;
import static java.util.Collections.reverse;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Future;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;

/**
 *
 * @author Kim Jersin <kij@trifork.dk>
 */
@Stateless
public class KeycloakMigrate {

    @Inject
    protected MigratorService migrator;

    @Inject
    protected GkcadmConfiguration configuration;

    @Inject
    protected ClientMigrationContext context;

    @Inject
    protected ProgressStream out;

    @Asynchronous
    public Future<Boolean> migrate(String location) throws Exception {
        configuration.setLocation(location);
        for (var realm : configuration.getRealms()) {
            migrator.migrate(realm, configuration);
        }
        return new AsyncResult<>(true);
    }

    @Asynchronous
    public Future<Boolean> clean(String location) {
        configuration.setLocation(location);
        SessionResource sessions = context.migrations().sessions();
        out.println("--------------------------------");
        List<String> realms = new ArrayList<>(configuration.getRealms());
        reverse(realms);
        realms.forEach(realm -> {
            UUID sessionId = Create.uuid(
                    sessions.createSession(new CreateSession(realm))
            );
            try {
                context.keycloak().realm(realm).remove();
                out.printf("Cleaned realm: '%s'\n", realm);
            } catch (NotFoundException ex) {
                // Ignore
            } finally {
                sessions.removeSession(sessionId);
            }
        });
        return new AsyncResult<>(true);
    }

    @Asynchronous
    public Future<Boolean> info(String location) {
        configuration.setLocation(location);
        for (var realm : configuration.getRealms()) {
            List<ScriptHistory> history = null;
            try {
                history = context.migrations().realms().scripts(realm).getScripts();
            } catch (NotFoundException ex) {
                // Ignore
            }

            out.printf("\nRealm: '%s'\n", realm);
            out.println("--------------------------------");
            if (history != null) {
                migrator.info(realm, configuration, history);
            } else {
                out.println("(Not under migration control)");
            }
        }
        return new AsyncResult<>(true);
    }
}
