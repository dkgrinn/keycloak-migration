package dk.grinn.keycloak.migration.core.migrate;

/*-
 * #%L
 * Keycloak : Migrate : Core
 * %%
 * Copyright (C) 2019 - 2021 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.migration.core.BaseJavaMigration;
import dk.grinn.keycloak.migration.core.exception.ChecksumValidationFailedException;
import dk.grinn.keycloak.migration.core.exception.KeycloakMigrationException;
import dk.grinn.keycloak.migration.entities.ScriptHistory;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MigrationValidatorTest {


    @Test
    void newStandardMigrationsShouldRun() {
        final var migration = new Migration("V01_01__New_migration", 1L);

        assertTrue(shouldRunMigration(migration, new MigrationValidator(Collections.emptyList())));
    }

    @Test
    void registeredStandardMigrationsShouldNotRun() {
        final var migration = new Migration("V01_01__migration", 1L);

        final List<ScriptHistory> history = List.of(history(migration));

        assertFalse(shouldRunMigration(migration, new MigrationValidator(history)));
    }

    @Test
    void registeredStandardMigrationsWihNullChecksumShouldNotRun() {
        final var migration = new Migration("V01_01__migration", null);

        final List<ScriptHistory> history = List.of(history(migration));

        assertFalse(shouldRunMigration(migration, new MigrationValidator(history)));
    }

    @Test
    void standardMigrationMayNotChangeChecksum() {
        final var migration = new Migration("V01_01__migration", 1L);

        final List<ScriptHistory> history = List.of(history(migration));

        migration.setChecksum(2L);

        final var migrationValidator = new MigrationValidator(history);
        assertThrows(ChecksumValidationFailedException.class, () -> shouldRunMigration(migration, migrationValidator));
    }

    @Test
    void standardMigrationMayNotChangeChecksumToNull() {
        final var migration = new Migration("V01_01__migration", 1L);

        final List<ScriptHistory> history = List.of(history(migration));

        migration.setChecksum(null);

        final var migrationValidator = new MigrationValidator(history);
        assertThrows(ChecksumValidationFailedException.class, () -> shouldRunMigration(migration, migrationValidator));
    }

    @Test
    void standardMigrationMayNotChangeChecksumFromNull() {
        final var migration = new Migration("V01_01__migration", null);

        final List<ScriptHistory> history = List.of(history(migration));

        migration.setChecksum(1L);

        final var migrationValidator = new MigrationValidator(history);
        assertThrows(KeycloakMigrationException.class, () -> shouldRunMigration(migration, migrationValidator));
    }

    @Test
    void repeatableMigrationShouldRunOnChangedChecksum() {
        final var migration = new Migration("R01_01__Repeatable_migration", 1L);

        final List<ScriptHistory> history = List.of(history(migration));

        migration.setChecksum(2L);

        assertTrue(shouldRunMigration(migration, new MigrationValidator(history)));
    }

    @Test
    void repeatableMigrationShouldRunOnIfChecksumIsNull() {
        final var migration = new Migration("R01_01__Repeatable_migration", null);

        final List<ScriptHistory> history = List.of(history(migration));

        assertTrue(shouldRunMigration(migration, new MigrationValidator(history)));
    }

    @Test
    void migrationsShouldRunInRegisteredOrder() {
        final var migration1 = new Migration("V01_01__migration", 1L);
        final var migration2 = new Migration("V01_02__migration", 2L);
        final var migration3 = new Migration("V01_03__migration", 3L);

        final List<ScriptHistory> history = List.of(history(migration1), history(migration2));

        final var migrationValidator = new MigrationValidator(history);
        assertFalse(shouldRunMigration(migration1, migrationValidator));
        assertFalse(shouldRunMigration(migration2, migrationValidator));
        assertTrue(shouldRunMigration(migration3, migrationValidator));
    }

    @Test
    void migrationsShouldNotRunInAnyNonRegisteredOrder() {
        final var migration1 = new Migration("V01_01__migration", 1L);
        final var migration2 = new Migration("V01_02__migration", 2L);
        final var migration3 = new Migration("V01_03__migration", 3L);

        final List<ScriptHistory> history = List.of(history(migration1), history(migration2));

        final var migrationValidator = new MigrationValidator(history);
        assertFalse(shouldRunMigration(migration1, migrationValidator));
        assertThrows(KeycloakMigrationException.class, () -> shouldRunMigration(migration3, migrationValidator));
    }

    private ScriptHistory history(Migration migration) {
        return new ScriptHistory(migration.getVersion().toString(), null, migration.getChecksum(), null, null, null);
    }

    private boolean shouldRunMigration(Migration migration, MigrationValidator migrationValidator) {
        return migrationValidator.shouldRunMigration(migration, "realm");
    }

    private static class Migration extends BaseJavaMigration {

        public Migration(String name, Long checksum) {
            super(null, name, checksum, Optional.empty());
        }

        @Override
        public void migrate() {
            System.out.println(getVersion() + " - " + getDescription());
        }
    }
}
