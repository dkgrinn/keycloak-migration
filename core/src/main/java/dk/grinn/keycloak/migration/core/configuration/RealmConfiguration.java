package dk.grinn.keycloak.migration.core.configuration;

/*-
 * #%L
 * Keycloak : Migrate : Core
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.admin.AbstractConfiguration;
import static java.util.Arrays.asList;
import javax.enterprise.inject.Produces;
import org.apache.commons.configuration2.CompositeConfiguration;
import org.apache.commons.configuration2.Configuration;

/**
 *
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
public class RealmConfiguration extends AbstractConfiguration {

    private CompositeConfiguration config;

    public RealmConfiguration() {
    }
    
    public RealmConfiguration(String location, String realm, Configuration rootConfig) {
        config = new CompositeConfiguration(asList(
                rootConfig.subset("location." + location + ".realm." + realm),
                rootConfig.subset("location." + location),
                rootConfig.subset("realm." + realm),
                rootConfig
        ));
    }

    @Produces
    public CustomRealmKey getCustomRealmKey() {
        return new CustomRealmKeyImpl();
    }

    @Override
    protected CompositeConfiguration getConfig() {
        return config;
    }

    /**
     *
     */
    private class CustomRealmKeyImpl implements CustomRealmKey {

        @Override
        public boolean isEnabled() {
            return Boolean.parseBoolean(config.getString("realmkey.enabled"));
        }

        @Override
        public boolean isReuseEnabled() {
            return Boolean.parseBoolean(config.getString("realmkey.reuse"));
        }

        @Override
        public String getName() {
            return config.getString("realmkey.keyname");
        }

        @Override
        public String getSubject() {
            return config.getString("realmkey.subject");
        }

        @Override
        public Long getPriority() {
            try {
                return Long.parseLong(config.getString("realmkey.priority"));
            } catch (NumberFormatException e) {
                return null;
            }
        }

        @Override
        public String getPrivateKey() {
            return config.getString("realmkey.privatekey");
        }

        @Override
        public String getCertificate() {
            return config.getString("realmkey.certificate");
        }

        @Override
        public String getCertificatePath() {
            return config.getString("realmkey.path");
        }

        @Override
        public char[] getCertificatePassword() {
            String password = config.getString("realmkey.password");
            return password != null ? password.toCharArray() : null;
        }

        @Override
        public String getCertificateAlias() {
            return config.getString("realmkey.alias");
        }

    }
}
