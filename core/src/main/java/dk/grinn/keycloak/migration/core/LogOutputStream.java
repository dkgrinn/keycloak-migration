package dk.grinn.keycloak.migration.core;

/*-
 * #%L
 * Keycloak : Migrate : Core
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Logger;

/**
 *
 * @author Kim Jersin <kij@trifork.dk>
 */
public class LogOutputStream extends OutputStream {

    private static final Logger LOG = Logger.getLogger(LogOutputStream.class.getName());

    private ByteArrayOutputStream ln = new ByteArrayOutputStream();

    public LogOutputStream() {
        LOG.info("Redirection output to logger");
    }
    
    @Override
    public void write(int b) {
        if (b == '\n') {
            if (ln.size() > 0) {
                LOG.info(() -> ln.toString());
            }
            ln = new ByteArrayOutputStream();
        } else {
            ln.write(b);
        }
    }
}
