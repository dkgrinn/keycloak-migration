package dk.grinn.keycloak.migration.core;

/*-
 * #%L
 * Keycloak : Migrate : Core
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */
import static java.lang.Math.min;
import java.util.Arrays;
import static java.util.Arrays.stream;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Version implements Comparable<Version> {

    private final MigrationType type;

    private final int[] versionArray;

    public Version(MigrationType type, int[] version) {
        this.type = type;
        this.versionArray = version;
    }

    public Version(MigrationType type, String[] version) {
        this.type = type;
        this.versionArray = stream(version)
                .mapToInt(Integer::valueOf)
                .toArray();
    }

    @Override
    public int compareTo(Version other) {
        if (versionArray == other.versionArray) {
            return 0;
        }
        if (versionArray == null || other.versionArray == null) {
            return versionArray == null ? -1 : 1;
        }

        int compareLength = min(versionArray.length, other.versionArray.length);
        if (compareLength > 0) {
            int i = 0;
            while (i < compareLength) {
                if (versionArray[i] != other.versionArray[i]) {
                    break;
                }
                i++;
            }
            if (i < compareLength) {
                int result = Integer.compare(versionArray[i], other.versionArray[i]);
                return (result != 0) ? result : type.compareTo(other.type);
            }
        }
        int result = versionArray.length - other.versionArray.length;
        return (result != 0) ? result : type.compareTo(other.type);
    }

    /**
     * Returns the migration type
     */
    public MigrationType getType() {
        return type;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Arrays.hashCode(this.versionArray);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Version other = (Version) obj;
        if (!Arrays.equals(this.versionArray, other.versionArray)) {
            return false;
        }
        return type.equals(other.type);
    }

    @Override
    public String toString() {
        return type.toString() + ":" + IntStream.of(versionArray)
                .mapToObj(Integer::toString)
                .collect(Collectors.joining("."));
    }
}
