package dk.grinn.keycloak.migration.annotation;

/*-
 * #%L
 * Keycloak : Migrate : Core
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
@Target(value = {ElementType.METHOD, ElementType.TYPE, ElementType.FIELD})
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
@RequestScoped
@Inherited
public @interface Migration {

    /**
     * (Optional) the version/repeatable string.
     * <p>
     * Examples:<ul>
     * <li><pre>V1 Initial</pre>
     * <li><pre>V20.07</pre>
     * <li><pre>V1.10.34 The latest update</pre>
     * <li><pre>R1.1 Update roles</pre>
     * </ul>
     * If omitted then the migration class name is used. The description can be
     * embedded into the version string (everything after the space is
     * considered a description).
     * <p>
     * The class name format with underscores are also allowed:<ul>
     * <li><pre>V1__Initial</pre>
     * <li><pre>V20_07</pre>
     * <li><pre>V1_10__34__The_latest_update</pre>
     * <li><pre>R1_1__Update_roles</pre>
     * </ul>
     *
     */
    public String value() default "";

    /**
     * (Optional) description.
     * <p>
     * If omitted then the description is extracted from the {@link #value()) or
     * as a last resort the migration class name is used.
     *
     * @return
     */
    public String description() default "";

    /**
     * (Optional) the migration scope.
     * <p>
     * If omitted the migration is used in any scope.
     *
     */
    public String scope() default "";
    
    /**
     * (Optional) the minimum required version of the migration module.
     * <p>
     * <b>NOTE</b>: 
     *
     */
    public String moduleVersion() default "";
}
