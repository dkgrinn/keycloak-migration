package dk.grinn.keycloak.migration.core;

import dk.grinn.keycloak.migration.annotation.Migration;
import dk.grinn.keycloak.migration.annotation.RequiresLock;

import java.util.Optional;
import java.util.regex.Pattern;

import static java.lang.String.format;
import static java.lang.String.join;
import static org.apache.commons.lang3.StringUtils.capitalize;
import static org.apache.commons.lang3.StringUtils.splitByCharacterTypeCamelCase;
import static org.apache.commons.text.WordUtils.uncapitalize;

/*-
 * #%L
 * Keycloak : Migrate : Core
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */
/**
 * Basis for all java based migrations (scripts and classpath implementations).
 * <p>
 * On creation the name (class name or script name) is split into it's parts
 * (version and description).
 *
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
public abstract class BaseJavaMigration implements JavaMigration {

    private static Pattern punct = Pattern.compile("\\.");

    private static Pattern space = Pattern.compile("\\s");

    private Version version;

    private String description;

    private String name;

    private String scope;
    
    private Optional<String> requiresLock;

    protected Long checksum;
    
    /**
     * Use the optional {@link Migration} annotation and class name.
     */
    protected BaseJavaMigration() {
        init(getClass().getAnnotation(Migration.class), null, getClass().getSimpleName(), null, initRequiresLock());
    }

    /**
     * Use the supplied class name and checksum.
     *
     */
    protected BaseJavaMigration(String defaultScope, String simpleName, Long checksum, Optional<String> requiresLock) {
        init(null, defaultScope, simpleName, checksum, requiresLock);
    }

    /**
     * Use the supplied class name and checksum.
     *
     */
    protected BaseJavaMigration(Migration annotation, String defaultScope, String simpleName, Long checksum, Optional<String> requiresLock) {
        init(annotation, defaultScope, simpleName, checksum, requiresLock);
    }

    private void init(Migration annotation, String defaultScope, String simpleName, Long checksum, Optional<String> requiresLock) {
        this.scope = defaultScope;
        this.name = simpleName;
        this.requiresLock = requiresLock;
        if (annotation == null || annotation.value().isBlank()) {
            // No annotation or no version information in the annotation - so use it from the class name
            int sepIndex = simpleName.indexOf("__");
            this.version = versionFrom(simpleName.substring(0, sepIndex));
            this.description = simpleName.substring(sepIndex + 2).replace('_', ' ');
        } else {
            // Annotated version value is present so use what are available
            // Relaxed parsing of the annotated value:
            // Both class name form with "__" as separator and the annotation form
            // where a space separator is supported. Even no description part is allowed.
            String[] parts = space.split(annotation.value().replace("__", " ").trim(), 2);
            this.version = versionFrom(parts[0]);
            if (parts.length == 2) {
                this.description = parts[1].trim();
            }
        }

        // Values set directly on the annotation always overrules
        if (annotation != null) {
            if (!annotation.scope().isBlank()) {
                this.scope = annotation.scope().trim();
            }
            if (!annotation.description().isBlank()) {
                this.description = annotation.description().trim();
            }
        }

        // If by now no description was found, then use the class name as description
        if (this.description == null) {
            this.description = capitalize(uncapitalize(
                    join(" ", splitByCharacterTypeCamelCase(simpleName))
            ));
        }

        this.checksum = checksum;
    }
    
    private Optional<String> initRequiresLock() {
        return getRequiresLock(this.getClass());
    }
    
    public static Optional<String> getRequiresLock(Class<? extends JavaMigration> cls) {
        try {
            var requiresLock = cls.getMethod("migrate").getAnnotation(RequiresLock.class);
            return requiresLock != null ? Optional.of(requiresLock.value()) : Optional.empty();
        } catch (NoSuchMethodException | SecurityException ex) {
            throw new IllegalStateException(ex);
        }
    }

    private Version versionFrom(String id) {
        return new Version(
                MigrationType.valueOf(id.substring(0, 1).toUpperCase()),
                punct.split(id.substring(1).replace('_', '.'))
        );
    }

    public Version getVersion() {
        return version;
    }

    public String getDescription() {
        return description;
    }

    public Long getChecksum() {
        return checksum;
    }

    public void setChecksum(Long checksum) {
        this.checksum = checksum;
    }

    @Override
    public String toString() {
        return format("%-8s %-10s - %s", 
                version.toString(),
                format("(%s)", scope != null ? scope : "<none>"),
                getDescription()
        );
    }

    public String getName() {
        return name;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public Optional<String> getRequiresLock() {
        return requiresLock;
    }
}
