package dk.grinn.keycloak.migration.core.migrate;

/*-
 * #%L
 * Keycloak : Migrate : Core
 * %%
 * Copyright (C) 2019 - 2021 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.admin.GkcadmConfiguration;
import dk.grinn.keycloak.migration.annotation.Migration;
import dk.grinn.keycloak.migration.core.BaseJavaMigration;
import dk.grinn.keycloak.migration.core.BaseJavaMigrationMapper;
import dk.grinn.keycloak.migration.core.ClientMigrationContext;
import dk.grinn.keycloak.migration.core.JavaMigration;
import dk.grinn.keycloak.migration.core.ModuleVersion;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.System.err;

@ApplicationScoped
public class MigrationScanner {

    @Inject
    protected Instance<JavaMigration> migrationBeans;

    @Inject
    protected ClientMigrationContext context;

    @Inject
    protected BaseJavaMigrationMapper mapper;

    public List<BaseJavaMigration> scan(GkcadmConfiguration gkcadmConfig) {
        Map<String, ModuleVersion> moduleVersions = new HashMap<>();

        // Instrument all the migrations
        int beanCount = 0;
        var migrations = new ArrayList<BaseJavaMigration>();

        for (var i = migrationBeans.iterator(); i.hasNext(); beanCount++) {

            var candidate = i.next();
            var annotation = candidate.getClass().getAnnotation(Migration.class);

            BaseJavaMigration migration = mapper.map(candidate);

            // Also needs to be in correct scope
            if (gkcadmConfig.containsScopeFor(migration)) {
                migrations.add(migration);
            }

            // If set then also update the required module version
            if (!annotation.moduleVersion().isEmpty()) {
                var parts = annotation.moduleVersion().split(":");
                final var moduleName = parts[0];
                final var version = new ModuleVersion(parts[1]);
                moduleVersions.putIfAbsent(moduleName, version);

                if (version.compareTo(moduleVersions.get(moduleName)) > 0) {
                    moduleVersions.put(moduleName, version);
                }
            }
        }

        context.setRequiredModuleVersions(moduleVersions);

        // Sanity check - if no beans where found then dump some info to the
        // developer of possible causes and solutions.
        if (beanCount == 0) {
            noBeansInfo();
        }

        migrations.sort(Comparator.comparing(BaseJavaMigration::getVersion));

        return migrations;
    }

    private void noBeansInfo() {
        err.println("\n########          NO MIGRATIONS DISCOVERED IN THE CLASSPATH          ########");
        err.println("########  If you have implemented a migration class and still see    ########");
        err.println("########  this message then it's properly caused by a missing        ########");
        err.println("########  META-INF/beans.xml file.                                   ########");
        err.println();
    }


}
