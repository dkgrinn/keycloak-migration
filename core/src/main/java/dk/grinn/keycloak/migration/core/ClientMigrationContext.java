package dk.grinn.keycloak.migration.core;

/*-
 * #%L
 * Keycloak : Migrate : Core
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */
import dk.grinn.keycloak.admin.GkcadmConfiguration;
import dk.grinn.keycloak.admin.ProgressStream;
import dk.grinn.keycloak.admin.boundary.InfoResource;
import dk.grinn.keycloak.migration.boundary.CreateSession;
import dk.grinn.keycloak.migration.boundary.MigrationResource;
import dk.grinn.keycloak.migration.boundary.RealmResource;
import dk.grinn.keycloak.migration.boundary.RealmScriptResource;
import dk.grinn.keycloak.migration.boundary.ScriptBegin;
import dk.grinn.keycloak.migration.boundary.ScriptCommit;
import dk.grinn.keycloak.migration.boundary.ScriptResource;
import dk.grinn.keycloak.migration.boundary.SessionResource;
import dk.grinn.keycloak.migration.entities.CreateRealmKey;
import dk.grinn.keycloak.migration.entities.ScriptHistory;
import dk.grinn.keycloak.migration.entities.ServerInfo;
import dk.grinn.keycloak.migration.entities.Session;
import static java.util.Collections.emptyMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import static java.util.stream.Collectors.toList;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import org.apache.http.conn.HttpHostConnectException;
import org.jboss.logging.Logger;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;

/**
 *
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
@RequestScoped
public class ClientMigrationContext {

    private static final Logger LOG = Logger.getLogger(ClientMigrationContext.class);

    public static final String GKCADM_URL_PATH = "realms/master/gkcadm";

    private final Map<String, Keycloak> clients;

    private Map<String, ModuleVersion> requiredModuleVersions;

    public ClientMigrationContext() {
        clients = new HashMap<>();
        requiredModuleVersions = emptyMap();
    }
    
    @Inject
    protected GkcadmConfiguration configuration;

    @Inject
    protected ProgressStream out;

    @PostConstruct
    public void init() {
        LOG.debug("Init:");
    }

    @PreDestroy
    public void close() {
        for (Keycloak kcClient : clients.values()) {
            kcClient.close();
        }
    }

    public Keycloak keycloak() {
        return adminKeycloak();
    }

    public Keycloak adminKeycloak() {
        return clients.computeIfAbsent("admin", key -> {
            // Build the connection
            out.printf("\nConnecting to: %s\n", configuration.getUrl());
            Keycloak keycloak;

            int connectRetry = configuration.getConnectRetry();
            do {
                keycloak = KeycloakBuilder.builder()
                        .serverUrl(configuration.getUrl())
                        .realm("master")
                        .username(configuration.getAdminUser())
                        .password(configuration.getAdminPassword())
                        .clientId("admin-cli")
                        .build();

                // Make sure to have a valid connection by requesting some info
                try {
                    var info = keycloak.serverInfo().getInfo();
                    var systemInfo = info.getSystemInfo();
                    if (connectRetry >= 0) {
                        if (hasModuleVersions(keycloak.proxy(InfoResource.class, uriBuilder("info").build()).info())) {
                            out.printf("Keycloak %s (%s) - %s (%s)\n",
                                    systemInfo.getVersion(), info.getProfileInfo().getName(),
                                    systemInfo.getJavaVm(), systemInfo.getJavaVersion()
                            );
                        } else {
                            keycloak.close();
                            keycloak = null;
                        }
                    }
                } catch (ProcessingException pex) {
                    if (pex.getCause() instanceof HttpHostConnectException) {
                        out.println(pex.getMessage());
                        keycloak = null;
                    } else {
                        throw pex;
                    }
                }
            } while (--connectRetry >= 0 && keycloak == null && sleep());

            if (keycloak == null) {
                throw new RuntimeException("Unable to connect to the Keycloak server!");
            }
            return keycloak;
        });
    }

    public void setRequiredModuleVersions(Map<String, ModuleVersion> requiredModuleVersions) {
        this.requiredModuleVersions = requiredModuleVersions;
    }

    /**
     * Main client resource entry point for all the migration functionality.
     *
     */
    @Produces
    public MigrationResource migrations() {
        return new MigrationResource() {
            @Override
            public SessionResource sessions() {
                final SessionResource resource = adminKeycloak().proxy(SessionResource.class,
                        uriBuilder("migrations/sessions").build()
                );
                return new SessionResource() {
                    @Override
                    public List<Session> getSessions(boolean activeOnly) {
                        return resource.getSessions(activeOnly);
                    }

                    @Override
                    public Session getSession(UUID sessionId) {
                        return resource.getSession(sessionId);
                    }

                    @Override
                    public ScriptResource scripts(UUID sessionId) {
                        final ScriptResource resource = adminKeycloak().proxy(ScriptResource.class,
                                uriBuilder("migrations/sessions/{sessionId}/scripts")
                                        .resolveTemplate("sessionId", sessionId)
                                        .build()
                        );
                        return new ScriptResource() {
                            @Override
                            public ScriptHistory getScript(int id, int rank) {
                                return resource.getScript(id, rank);
                            }

                            @Override
                            public List<ScriptHistory> getScripts() {
                                return resource.getScripts();
                            }

                            @Override
                            public Response begin(ScriptBegin args, boolean repeatable) {
                                return resource.begin(args, repeatable);
                            }

                            @Override
                            public Response commit(int id, int rank, ScriptCommit args) {
                                return resource.commit(id, rank, args);
                            }

                            @Override
                            public Response abort(int id, int rank) {
                                return resource.abort(id, rank);
                            }

                            @Override
                            public Response open(String version) {
                                return resource.open(version);
                            }
                        };
                    }

                    @Override
                    public Response setRealmKey(UUID sessionId, CreateRealmKey createRealmKey) {
                        return resource.setRealmKey(sessionId, createRealmKey);
                    }

                    @Override
                    public Response disableRsaGeneratedKey(UUID sessionId, String realmId) {
                        return resource.disableRsaGeneratedKey(sessionId, realmId);
                    }

                    @Override
                    public Response createSession(CreateSession args) {
                        return resource.createSession(args);
                    }

                    @Override
                    public Response releaseSession(UUID sessionId) {
                        return resource.releaseSession(sessionId);
                    }

                    @Override
                    public Response removeSession(UUID sessionId) {
                        return resource.removeSession(sessionId);
                    }
                };
            }

            @Override
            public dk.grinn.keycloak.migration.boundary.RealmResource realms() {
                final RealmResource resource = adminKeycloak().proxy(RealmResource.class,
                        uriBuilder("migrations/realms").build()
                );
                return new RealmResource() {
                    @Override
                    public Response abortSession(String realm) {
                        return resource.abortSession(realm);
                    }

                    @Override
                    public List<Session> getAllRealmScripts() {
                        return resource.getAllRealmScripts();
                    }

                    @Override
                    public RealmScriptResource scripts(String realm) {
                        final RealmScriptResource resource = adminKeycloak().proxy(RealmScriptResource.class,
                                uriBuilder("migrations/realms/{realm}/scripts")
                                        .resolveTemplate("realm", realm)
                                        .build()
                        );
                        return new RealmScriptResource() {
                            @Override
                            public List<ScriptHistory> getScripts() {
                                return resource.getScripts();
                            }

                            @Override
                            public ScriptHistory getScript(String version) {
                                return resource.getScript(version);
                            }
                        };
                    }
                };
            }

            @Override
            public Response runMigrations() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
    }

    public boolean hasModuleVersions(List<ServerInfo> serverInfos) {
        var res = true;
        for (var entry : requiredModuleVersions.entrySet()) {
            var serverVersions = serverInfos.stream()
                    .filter(info -> entry.getKey().equals(info.getName()))
                    .map(ServerInfo::getVersion)
                    .collect(toList());

            // The referenced module most exist on the server
            if (!serverVersions.isEmpty()) {
                for (var version : serverVersions) {
                    if (entry.getValue().compareToStringValue(version) > 0) {
                        out.printf(
                                "Expected at least version %s, but found version %s\n",
                                entry.getValue(), version
                        );
                        res = false;
                    }
                }
            } else {
                out.printf("Module versions check: %s not found on server\n", entry.getKey());
                res = false;
            }
        }

        return res;
    }

    private UriBuilder uriBuilder(String resourcePath) {
        String url = configuration.getUrl().trim();
        return UriBuilder.fromUri(url + (url.endsWith("/") ? "" : "/") + GKCADM_URL_PATH + "/" + resourcePath);
    }

    private boolean sleep() {
        out.println("Unable to connect - but will keep trying.... \n");
        try {
            Thread.sleep(29989L);
        } catch (InterruptedException iex) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(iex);
        }
        return true;
    }
}
