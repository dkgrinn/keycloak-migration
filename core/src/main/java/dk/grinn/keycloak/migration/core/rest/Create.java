package dk.grinn.keycloak.migration.core.rest;

/*-
 * #%L
 * Keycloak : Migrate : Core
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.migration.entities.ScriptHistoryKey;

import javax.ws.rs.client.ResponseProcessingException;
import javax.ws.rs.core.Response;
import java.nio.file.Paths;
import java.util.UUID;

import static org.jboss.resteasy.util.HttpResponseCodes.SC_CREATED;

/**
 * Create endpoint helper for REST calls
 */
public class Create {

    private Create() {
        // Utitlity response parsing class for creations
    }

    /**
     * Extracts the UUID from a response.
     * A ResponseProcessingException is thrown if the response is not 201 - Created
     */
    public static UUID uuid(Response response) {
        if (response.getStatus() == SC_CREATED) {
            return UUID.fromString(Paths.get(response.getLocation().getPath()).getFileName().toString());
        } else {
            throw new ResponseProcessingException(response, "Expected: 201 - Created, but got: " 
                    + response.getStatusInfo().getStatusCode() + " " + response.getStatusInfo().getReasonPhrase()
            );
        }
    }

    /**
     * Extracts a script historyKey from a response.
     * A ResponseProcessingException is thrown if the response is not 201 - Created
     */
    public static ScriptHistoryKey key(Response response) {
        if (response.getStatus() == SC_CREATED) {
            return new ScriptHistoryKey(Paths.get(response.getLocation().getPath()));
        } else {
            throw new ResponseProcessingException(response, "Expected: 201 - Created");
        }
    }
}
