package dk.grinn.keycloak.migration.core.migrate;

/*-
 * #%L
 * Keycloak : Migrate : Core
 * %%
 * Copyright (C) 2019 - 2021 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.migration.core.BaseJavaMigration;
import dk.grinn.keycloak.migration.core.MigrationType;
import dk.grinn.keycloak.migration.core.exception.ChecksumValidationFailedException;
import dk.grinn.keycloak.migration.core.exception.KeycloakMigrationException;
import dk.grinn.keycloak.migration.entities.ScriptHistory;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import static java.lang.String.format;

public class MigrationValidator {

    private final Iterator<ScriptHistory> historyIterator;

    public MigrationValidator(List<ScriptHistory> history) {
        this.historyIterator = history.listIterator();
    }

    public boolean shouldRunMigration(BaseJavaMigration migration, String realm) {
        if (historyIterator.hasNext()) {
            ScriptHistory scriptHistory = historyIterator.next();

            // Correct order?
            if (!Objects.equals(scriptHistory.getVersion(), migration.getVersion().toString())) {
                throw new KeycloakMigrationException(realm, format("Did not expect %s to be in the list here. It should be %s", migration, scriptHistory));
            }

            // New checksum?
            boolean standardMigration = migration.getVersion().getType().equals(MigrationType.V);
            boolean checksumMatch = checksumMatch(migration, scriptHistory, realm);

            if (checksumMatch) {
                // do not run migration if checksum is unchanged
                return false;
            } else if (standardMigration) {
                // Only repeatable migrations are allowed to run more than once, when checksum is different
                throw new ChecksumValidationFailedException(migration, scriptHistory);
            }
        }

        return true;
    }


    /**
     * Verifies if the checksum has changed given the migration and the latest
     * one from history
     */
    private boolean checksumMatch(BaseJavaMigration migration, ScriptHistory lastExecutedMigration, String realm) {
        if (lastExecutedMigration.getChecksum() == null) {
            if (migration.getVersion().getType().equals(MigrationType.V)) {
                // If the previous one had a null-checksum and the new one doesn't => throw an exception
                if (migration.getChecksum() != null) {
                    throw new KeycloakMigrationException(realm, String.format("Checksum verification failed. It looks like you are trying to run"
                                    + " a migration that previously had no checksum but now has. Original migration: %s, new migration: %s",
                            lastExecutedMigration, migration));
                }

                // If both checksums are null, and it's a V-type: Assume unchanged
                return true;
            } else if (migration.getVersion().getType().equals(MigrationType.R)) {
                // For the repeating migrations -> assume changed as long as the checksum is null
                // Do not validate the new checksum; it may change from null to a real value
                return false;
            }
        }

        // Simple comparison
        return lastExecutedMigration.getChecksum().equals(migration.getChecksum());
    }

}
