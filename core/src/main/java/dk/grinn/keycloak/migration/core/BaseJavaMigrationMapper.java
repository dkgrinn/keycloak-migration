package dk.grinn.keycloak.migration.core;

/*-
 * #%L
 * Keycloak : Migrate : Core
 * %%
 * Copyright (C) 2019 - 2021 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.migration.annotation.Migration;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import java.util.Map;

import static dk.grinn.keycloak.migration.core.BaseJavaMigration.getRequiresLock;

@ApplicationScoped
public class BaseJavaMigrationMapper {

    protected final Map<String, Long> checksums;

    protected final ScopeMapper scopeMapper;

    @Inject
    public BaseJavaMigrationMapper(
            @Named("checksums") Map<String, Long> checksums,
            @NotNull Instance<ScopeMapper> scopeMapperInstance
    ) {
        this.checksums = checksums;
        this.scopeMapper = scopeMapperInstance.isResolvable()
                ? scopeMapperInstance.get()
                : c -> null;
    }

    public BaseJavaMigration map(JavaMigration bean) {
        var annotation = bean.getClass().getAnnotation(Migration.class);
        if (bean instanceof BaseJavaMigration) {
            var migration = (BaseJavaMigration) bean;
            migration.setChecksum(getChecksum(bean));
            migration.setScope(annotation.scope().isBlank() ? scopeMapper.scopeFrom(bean.getClass()) : annotation.scope());
            return migration;
        } else {
            return wrapBean(bean);
        }
    }

    private BeanWrappedMigration wrapBean(JavaMigration bean) {
        if (bean instanceof RollbackAware) {
            return new BeanWrappedMigrationWithRollback(bean,
                    getChecksum(bean),
                    scopeMapper, getRequiresLock(bean.getClass())
            );
        } else {
            return new BeanWrappedMigration(bean,
                    getChecksum(bean),
                    scopeMapper, getRequiresLock(bean.getClass())
            );
        }
    }

    private Long getChecksum(JavaMigration candidate) {
        return checksums.get(candidate.getClass().getSimpleName() + ".java");
    }
}
