package dk.grinn.keycloak.migration.core.migrate;

/*-
 * #%L
 * Keycloak : Migrate : Core
 * %%
 * Copyright (C) 2019 - 2020 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.admin.GkcadmConfiguration;
import dk.grinn.keycloak.admin.ProgressStream;
import dk.grinn.keycloak.migration.core.exception.KeycloakMigrationException;
import dk.grinn.keycloak.migration.core.exception.KeycloakMigrationRollbackFailedException;
import static dk.grinn.keycloak.migration.core.migrate.Migrator.migrationExecuted;
import dk.grinn.keycloak.migration.entities.ScriptHistory;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import org.jboss.logging.Logger;

/**
 *
 * @author Kim Jersin <kij@trifork.dk>
 */
@RequestScoped
public class MigratorService {

    private static final Logger LOG = Logger.getLogger(MigratorService.class);
    
    @Inject
    private Migrator migrator;

    @Inject
    protected ProgressStream out;

    public void migrate(String realm, GkcadmConfiguration gkcadmConfig) throws Exception {
        try {
            migrator.setContext(realm, gkcadmConfig).migrate();
        } catch (KeycloakMigrationRollbackFailedException rollbackFailed) {
            LOG.error("FATAL: A migration failed, AND ROLLBACK FAILED! Please check the logs and make sure your system is in a valid state");
            throw rollbackFailed;
        } catch (KeycloakMigrationException migrationException) {
            LOG.error("A migration failed");
            throw migrationException;
        }
    }

    public void verify(String realm, GkcadmConfiguration gkcadmConfig) {
        migrator.setContext(realm, gkcadmConfig).verify();
    }

    public void info(String realm, GkcadmConfiguration gkcadmConfig, List<ScriptHistory> history) {
        for (var migration : migrator.setContext(realm, gkcadmConfig).scan()) {
            if (migrationExecuted(migration, history)) {
                out.printf("[+] %s\n", migration);
            } else {
                out.printf("[-] %s\n", migration);
            }
        }
    }
}
