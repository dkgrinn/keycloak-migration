package dk.grinn.keycloak.migration.core.migrate;

/*-
 * #%L
 * Keycloak : Migrate : Core
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.admin.GkcadmConfiguration;
import dk.grinn.keycloak.admin.ProgressStream;
import dk.grinn.keycloak.migration.boundary.CreateSession;
import dk.grinn.keycloak.migration.boundary.ScriptBegin;
import dk.grinn.keycloak.migration.boundary.ScriptCommit;
import dk.grinn.keycloak.migration.boundary.ScriptResource;
import dk.grinn.keycloak.migration.boundary.SessionResource;
import dk.grinn.keycloak.migration.core.BaseJavaMigration;
import dk.grinn.keycloak.migration.core.ClientMigrationContext;
import dk.grinn.keycloak.migration.core.MigrationType;
import dk.grinn.keycloak.migration.core.RollbackAware;
import dk.grinn.keycloak.migration.core.Version;
import dk.grinn.keycloak.migration.core.configuration.RealmConfiguration;
import dk.grinn.keycloak.migration.core.exception.ChecksumValidationFailedException;
import dk.grinn.keycloak.migration.core.exception.KeycloakMigrationException;
import dk.grinn.keycloak.migration.core.exception.KeycloakMigrationRollbackFailedException;
import dk.grinn.keycloak.migration.core.rest.Create;
import dk.grinn.keycloak.migration.entities.CreateRealmKey;
import dk.grinn.keycloak.migration.entities.ScriptHistory;
import dk.grinn.keycloak.migration.entities.ScriptHistoryKey;
import org.jboss.logging.Logger;
import org.keycloak.admin.client.resource.AuthenticationManagementResource;
import org.keycloak.admin.client.resource.ClientScopesResource;
import org.keycloak.admin.client.resource.ClientsResource;
import org.keycloak.admin.client.resource.ComponentsResource;
import org.keycloak.admin.client.resource.GroupsResource;
import org.keycloak.admin.client.resource.IdentityProvidersResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.RealmRepresentation;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.NotFoundException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import static java.lang.String.format;
import static java.lang.System.currentTimeMillis;
import static java.util.Optional.empty;
import static java.util.Optional.of;

/**
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
@ApplicationScoped
public class Migrator {

    private static final Logger LOG = Logger.getLogger(Migrator.class);
    public static final long FAILURE_CHECKSUM = -1L;

    private ByteArrayOutputStream resultBuffer;

    private Optional<Lock> realmLock;

    /**
     * Active getRealmResource.
     */
    private String realm;

    private RealmConfiguration config;

    private GkcadmConfiguration gkcadmConfig;

    @Inject
    protected ProgressStream out;

    @Inject
    protected ClientMigrationContext context;

    @Inject
    protected RealmService realms;

    @Inject
    protected MigrationScanner migrationScanner;

    public Migrator setContext(String realm, GkcadmConfiguration gkcadmConfig) {
        this.realm = realm;
        this.gkcadmConfig = gkcadmConfig;
        this.config = new RealmConfiguration(gkcadmConfig.getLocation(), realm, gkcadmConfig.getRootConfig());
        return this;
    }

    @Produces
    @Named("result")
    @RequestScoped
    public PrintStream getMigrationResultOutput() {
        return new PrintStream(resultBuffer, true);
    }

    @Produces
    @RequestScoped
    public RealmResource getRealmResource() {
        return context.keycloak().realm(realm);
    }

    @Produces
    @RequestScoped
    public RolesResource getRolesResource() {
        return getRealmResource().roles();
    }

    @Produces
    @RequestScoped
    public UsersResource getUsersResource() {
        return getRealmResource().users();
    }

    @Produces
    @RequestScoped
    public GroupsResource getGroupsResource() {
        return getRealmResource().groups();
    }

    @Produces
    @RequestScoped
    public ComponentsResource getComponentsResource() {
        return getRealmResource().components();
    }

    @Produces
    @RequestScoped
    public ClientsResource getClientsResource() {
        return getRealmResource().clients();
    }

    @Produces
    @RequestScoped
    public ClientScopesResource getClientScopesResource() {
        return getRealmResource().clientScopes();
    }

    @Produces
    @RequestScoped
    public IdentityProvidersResource getIdentityProvidersResource() {
        return getRealmResource().identityProviders();
    }

    @Produces
    @RequestScoped
    public AuthenticationManagementResource getFlowsResource() {
        return getRealmResource().flows();
    }

    @Produces
    @RequestScoped
    @Named("realm")
    public String getRealm() {
        return realm;
    }

    @Produces
    @RequestScoped
    public RealmConfiguration getConfig() {
        if (config == null) {
            throw new IllegalStateException("Realm configuration not found");
        }
        return config;
    }

    public void verify() {
        scan();

        out.printf("\nVerify migrations on realm `%s`\n", realm);
        out.println("-----------------------------------------------");
    }

    /**
     * Runs the migrations.
     */
    public void migrate() throws IOException {
        realmLock = empty();
        resultBuffer = new ByteArrayOutputStream();
        var migrations = scan();

        // Make sure that the getRealmResource exists.
        createRealm();
        out.printf("\nExecuting migrations on realm `%s`\n", realm);
        out.println("-----------------------------------------------");

        // Lock getRealmResource for migrations
        SessionResource sessions = context.migrations().sessions();
        UUID sessionId = Create.uuid(sessions.createSession(new CreateSession(realm)));

        try {
            ScriptResource scripts = sessions.scripts(sessionId);
            List<ScriptHistory> history = scripts.getScripts();

            if (validateAndRunMigrations(migrations, history, scripts) == 0) {
                out.println("(No changes)");
            }
        } catch (ChecksumValidationFailedException cvfe) {
            out.println();
            LOG.error("Migrations failed!");
            out.println("Checksums of a migration does not match");
            out.printf("Migration from history: %s\n", cvfe.getLastExecutedMigration().toString());
            out.printf("Tried to execute the following migration: %s\n", cvfe.getMigration().toString());
        } finally {
            realmLock.ifPresent(Lock::release);
            sessions.releaseSession(sessionId);
            resultBuffer.writeTo(out.asOutputStream());
            out.flush();
        }
    }

    public List<BaseJavaMigration> scan() {
        return this.migrationScanner.scan(gkcadmConfig);
    }

    /**
     * Validates the migration by checking type and checksum, and executes the
     * migration if the conditions are met 1. If the migration is of the type
     * "V" (a normal migration): -> Run if it has not migrate -> Skip if the
     * checksum has not changed -> Throws a ChecksumValidationFailedException if
     * the checksum has changed since the last migrate 2. If the migration is of
     * the type "R" (repeatable migration): -> Run if it has not migrate -> Run
     * if the checksum has changed -> Ignore if it has migrate and the checksum
     * is identical 3. The order is correct -> If the order is not correct a
     * KeycloakMigrationException is thrown
     */
    private int validateAndRunMigrations(List<BaseJavaMigration> migrations, List<ScriptHistory> history, ScriptResource scripts) {
        int count = 0;
        final var migrationValidator = new MigrationValidator(history);
        for (BaseJavaMigration migration : migrations) {
            if (migrationValidator.shouldRunMigration(migration, realm)) {
                count += runMigration(scripts, migration);
            }
        }
        return count;
    }

    public static boolean migrationExecuted(BaseJavaMigration migration, List<ScriptHistory> history) {
        return history.stream().anyMatch(scriptHistory -> scriptHistory.getScript().equals(migration.getName()));
    }

    private int runMigration(ScriptResource scripts, BaseJavaMigration migration) throws KeycloakMigrationException {
        out.printf("[ ] %s", migration);
        Version version = migration.getVersion();
        boolean isRepeatable = version.getType().equals(MigrationType.R);
        ScriptHistoryKey key = Create.key(
                scripts.begin(new ScriptBegin(version.toString(), migration.getDescription(), "JAVA"), isRepeatable)
        );
        long start = currentTimeMillis();
        boolean migrationSuccessfull = false;
        try {
            migration.getRequiresLock().ifPresent(this::setLock);
            migration.migrate();
            migrationSuccessfull = true;
            out.printf("\r[+] %s\n", migration);
        } catch (Exception e) {
            handleError(scripts, migration, isRepeatable, key, e);
        } finally {
            if (migrationSuccessfull) {
                scripts.commit(key.getId(), key.getRank(), new ScriptCommit(
                        migration.getName(), migration.getChecksum(), (int) (currentTimeMillis() - start)
                ));
            }
        }
        return 1;
    }

    private void handleError(ScriptResource scripts, BaseJavaMigration migration, boolean isRepeatable, ScriptHistoryKey key, Exception e) {
        LOG.errorf("Failed to apply migration: %s", e.getMessage());
        LOG.error("Caused by", e);
        boolean isRolledBack = false;

        try {
            if (migration instanceof RollbackAware) {
                ((RollbackAware) migration).rollback();
                isRolledBack = true;
            }
        } catch (Exception rollbackException) {
            LOG.errorf("Rollback failed: %s", rollbackException.getMessage());
            LOG.error("Caused by", rollbackException);
            throw new KeycloakMigrationRollbackFailedException(realm, format("Migrations failed: %s", e.getMessage()), e);
        } finally {
            if (isRepeatable) {
                handleFailedRepeatableMigration(scripts, migration, key, isRolledBack);
            } else {
                scripts.abort(key.getId(), key.getRank());
            }
        }

        throw new KeycloakMigrationException(realm, format("Migrations failed: %s", e.getMessage()), e);
    }

    private void handleFailedRepeatableMigration(ScriptResource scripts, BaseJavaMigration migration, ScriptHistoryKey key, boolean isRolledBack) {
        ScriptHistory scriptHistory = scripts.getScript(key.getId(), key.getRank());

        if (scriptHistory != null && scriptHistory.getChecksum() != null) {
            long checksum = isRolledBack
                    ? scriptHistory.getChecksum()
                    : FAILURE_CHECKSUM;

            scripts.commit(key.getId(), key.getRank(), new ScriptCommit(
                    migration.getName(), checksum, scriptHistory.getExecutionTime())
            );
        } else {
            scripts.abort(key.getId(), key.getRank());
        }
    }

    private void setLock(String lockType) {
        if (lockType.equals("realm")) {
            if (realmLock.isEmpty()) {
                realmLock = of(realms.lock());
            }
        } else {
            throw new KeycloakMigrationException(realm, "Unknown lock type: '" + lockType + "'");
        }
    }

    private void createRealm() {
        // Check realm, and create if not exists
        try {
            context.adminKeycloak().realm(realm).toRepresentation();
        } catch (NotFoundException ex) {
            var rep = new RealmRepresentation();
            rep.setRealm(realm);
            context.adminKeycloak().realms().create(rep);

            var key = config.getCustomRealmKey();
            if (key.isEnabled()) {
                out.printf("Using custom realm key %s\n",
                        key.isReuseEnabled() ? "- set to resuse key from a possible previous run." : ""
                );
                setRealmKey();
            }
        }
    }

    private void setRealmKey() {
        var sessions = context.migrations().sessions();
        UUID sessionId = Create.uuid(
                sessions.createSession(new CreateSession(realm))
        );
        try {
            String realmId = context.adminKeycloak().realm(realm).toRepresentation().getId();

            var key = config.getCustomRealmKey();
            CreateRealmKey create = new CreateRealmKey(
                    realmId,
                    Objects.requireNonNullElse(key.getName(), "rsa"),
                    Objects.requireNonNullElse(key.getSubject(), "CN=" + realm),
                    Objects.requireNonNullElse(key.getPriority(), 100L),
                    key.getPrivateKey(),
                    key.getCertificate(),
                    key.getCertificatePath(),
                    key.getCertificatePassword(),
                    key.getCertificateAlias(),
                    key.isEnabled());

            sessions.setRealmKey(sessionId, create);
            sessions.disableRsaGeneratedKey(sessionId, realmId);
        } finally {
            sessions.removeSession(sessionId);
        }
    }

}
