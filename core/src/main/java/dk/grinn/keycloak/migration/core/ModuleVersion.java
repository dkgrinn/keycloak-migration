package dk.grinn.keycloak.migration.core;

/*-
 * #%L
 * Keycloak : Migrate : Core
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import java.util.Arrays;
import java.util.regex.Pattern;

import static java.util.Arrays.compare;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;

/**
 *
 * @author Kim Jersin <kij@trifork.dk>
 */
public class ModuleVersion implements Comparable<ModuleVersion> {

    private static final Pattern dash = Pattern.compile("\\-");

    private static final Pattern punct = Pattern.compile("\\.");

    private final int[] version;
    
    public static ModuleVersion of(String version) {
        return new ModuleVersion(version);
    }

    public ModuleVersion(int[] version) {
        this.version = version;
    }

    public ModuleVersion(String version) {
        this(punct.split(dash.split(version)[0]));
    }

    public ModuleVersion(String[] version) {
        this(stream(version)
                .mapToInt(Integer::valueOf)
                .toArray()
        );
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Arrays.hashCode(this.version);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ModuleVersion other = (ModuleVersion) obj;
        return Arrays.equals(this.version, other.version);
    }

    @Override
    public int compareTo(ModuleVersion o) {
        return compare(this.version, o.version);
    }

    public int compareToStringValue(String version) {
        return compareTo(new ModuleVersion(version));
    }

    @Override
    public String toString() {
        return stream(version)
                .mapToObj(Integer::toString)
                .collect(joining("."));
    }
}
