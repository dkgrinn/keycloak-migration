package dk.grinn.keycloak.admin;

/*-
 * #%L
 * Keycloak : Migrate : Core
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.admin.boundary.GkcAdmResource;
import dk.grinn.keycloak.admin.boundary.InfoResource;
import dk.grinn.keycloak.migration.boundary.MigrationResource;
import dk.grinn.keycloak.migration.core.ClientMigrationContext;
import static dk.grinn.keycloak.migration.core.ClientMigrationContext.GKCADM_URL_PATH;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.ws.rs.core.UriBuilder;

/**
 *
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
@RequestScoped
public class ClientGkcadmContext {

    @Inject
    protected GkcadmConfiguration configuration;
    
    @Inject
    protected ProgressStream out;

    @Inject
    protected ClientMigrationContext migrationContext;
    
    @Produces
    public GkcAdmResource getGkcAdmResource() {
        return new GkcAdmResource() {
            @Override
            public MigrationResource migrations() {
                return migrationContext.migrations();
            }

            @Override
            public InfoResource infoResource() {
                return migrationContext.adminKeycloak().proxy(
                        InfoResource.class, 
                        uriBuilder("info").build()
                );
            }
        };
    }

    private UriBuilder uriBuilder(String resourcePath) {
        String url = configuration.getUrl().trim();
        return UriBuilder.fromUri(url + (url.endsWith("/") ? "" : "/") + GKCADM_URL_PATH + "/" + resourcePath);
    }
}
