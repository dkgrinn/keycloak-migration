package dk.grinn.keycloak.admin;

/*-
 * #%L
 * Keycloak : Migrate : Core
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.migration.core.configuration.MigrateConfiguration;
import java.util.List;
import org.apache.commons.configuration2.CompositeConfiguration;
import org.apache.commons.configuration2.Configuration;

/**
 *
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
public abstract class AbstractConfiguration implements MigrateConfiguration {

    protected abstract CompositeConfiguration getConfig();

    protected AbstractConfiguration() {
    }

    @Override
    public String getHello() {
        return getConfig().getString("hello");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getRealms() {
        return getConfig().getList(String.class, "realms");
    }

    @Override
    public Configuration getPlaceholders() {
        return getConfig().subset("placeholders");
    }

    @Override
    public Configuration subset(String... subkeys) {
        return getConfig().subset(String.join(".", subkeys));
    }

    @Override
    public Configuration subset(String subkey, String[] subkeys) {
        return getConfig().subset(subkey + (subkeys.length > 0 ? "." : "") + String.join(".", subkeys));
    }

    @Override
    public String getUrl() {
        return getConfig().getString("url");
    }

    @Override
    public String getUsername() {
        return getConfig().getString("username");
    }

    @Override
    public String getPassword() {
        return getConfig().getString("password");
    }

    @Override
    public String getClientId() {
        return getConfig().getString("client.id");
    }

    @Override
    public String getAdminUser() {
        return getConfig().getString("admin.username");
    }

    @Override
    public String getAdminPassword() {
        return getConfig().getString("admin.password");
    }

    @Override
    public boolean containsKey(String key) {
        return getConfig().containsKey(key);
    }
    
}
