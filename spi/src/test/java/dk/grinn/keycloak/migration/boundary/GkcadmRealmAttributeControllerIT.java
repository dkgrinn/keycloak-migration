package dk.grinn.keycloak.migration.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Spi
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.migration.test.EntityManagerExtension;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * 
 * @author @author Nicolai Gjøderum &lt;nig-trifork@gmail.com&gt;
 */
@ExtendWith(EntityManagerExtension.class)
class GkcadmRealmAttributeControllerIT {

    @Test
    void putAttribute_firstTime_returnsNull(GkcadmRealmAttributeController controller){
        String realm = RandomStringUtils.randomAlphanumeric(15);
        String name = RandomStringUtils.randomAlphanumeric(15);
        String value = RandomStringUtils.randomAlphanumeric(15);

        String previous = controller.putAttribute(realm, name, value);
        assertNull(previous);
    }

    @Test
    void putAttribute_SecondTime_returnsPrevious(GkcadmRealmAttributeController controller){
        String realm = RandomStringUtils.randomAlphanumeric(15);
        String name = RandomStringUtils.randomAlphanumeric(15);
        String firstValue = "first";
        String secondValue = "second";

        controller.putAttribute(realm, name, firstValue);

        String previous = controller.putAttribute(realm, name, secondValue);
        assertEquals(firstValue, previous);
    }

    @Test
    void getAttribute_hasValue_returnsValue(GkcadmRealmAttributeController controller){
        String realm = RandomStringUtils.randomAlphanumeric(15);
        String name = RandomStringUtils.randomAlphanumeric(15);
        String value = RandomStringUtils.randomAlphanumeric(15);

        controller.putAttribute(realm, name, value);

        assertEquals(value, controller.getAttribute(realm, name));
    }

    @Test
    void getAttribute_hasNoValue_returnsNull(GkcadmRealmAttributeController controller){
        String realm = RandomStringUtils.randomAlphanumeric(15);
        String name = RandomStringUtils.randomAlphanumeric(15);

        assertNull(controller.getAttribute(realm, name));
    }
}
