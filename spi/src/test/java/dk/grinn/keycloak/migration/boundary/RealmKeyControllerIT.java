package dk.grinn.keycloak.migration.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Spi
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */


import dk.grinn.keycloak.migration.entities.CreateRealmKey;
import dk.grinn.keycloak.migration.test.EntityManagerExtension;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.component.ComponentModel;
import org.keycloak.models.RealmModel;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.UUID;

import static dk.grinn.keycloak.migration.boundary.RealmKeyController.CERTIFICATE_CONFIG_KEY;
import static dk.grinn.keycloak.migration.boundary.RealmKeyController.PRIVATE_KEY_CONFIG_KEY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author @author Nicolai Gjøderum &lt;nig-trifor@gmail.com&gt;
 */
@ExtendWith(EntityManagerExtension.class)
@ExtendWith(MockitoExtension.class)
class RealmKeyControllerIT {

    private String realmId;

    @Captor
    ArgumentCaptor<ComponentModel> componentModelArgumentCaptor;

    @Mock
    private RealmModel model;

    @BeforeEach
    void setup() {
        String realmName = RandomStringUtils.randomAlphanumeric(15);
        realmId = UUID.randomUUID().toString();

        when(model.getName()).thenReturn(realmName);
        when(model.getId()).thenReturn(realmId);
    }

    @Test
    void setRealmKey_pkAndCertSupplied_usesSupplied(RealmKeyController controller) {
        String privateKey = RandomStringUtils.randomAscii(50);
        String certificate = RandomStringUtils.randomAscii(50);

        CreateRealmKey create = createRealmKeyFromEnv(privateKey, certificate, "rsa", 100L, false);

        controller.setRealmKey(model, create);

        verify(model).addComponentModel(componentModelArgumentCaptor.capture());

        ComponentModel component = componentModelArgumentCaptor.getValue();

        assertEquals(privateKey, component.getConfig().getFirst(PRIVATE_KEY_CONFIG_KEY));
        assertEquals(certificate, component.getConfig().getFirst(CERTIFICATE_CONFIG_KEY));
    }

    private CreateRealmKey createRealmKeyFromEnv(String privateKey, String certificate, String rsa, long l, boolean b) {
        return new CreateRealmKey(realmId, rsa, "CN=test", l, privateKey, certificate, null, null, null, b);
    }

    @Test
    void setRealmKey_pkAndCertSuppliedAndNoPreviousPresent_GeneratesNewKey(RealmKeyController controller) {
        CreateRealmKey create = new CreateRealmKey(realmId, "rsa", "CN=test", 100L);

        controller.setRealmKey(model, create);

        verify(model).addComponentModel(componentModelArgumentCaptor.capture());

        ComponentModel component = componentModelArgumentCaptor.getValue();

        assertEquals("rsa", component.getName());
        assertEquals(realmId, component.getParentId());
        assertNotNull(component.getConfig().getFirst(PRIVATE_KEY_CONFIG_KEY));
        assertNotNull(component.getConfig().getFirst(CERTIFICATE_CONFIG_KEY));
    }

    @Test
    void setRealmKey_GeneratesNewKeyForEachRealm(RealmKeyController controller) {

        String realmName2 = RandomStringUtils.randomAlphanumeric(15);
        String realmId2 = UUID.randomUUID().toString();

        RealmModel model2 = mock(RealmModel.class);
        when(model2.getName()).thenReturn(realmName2);
        when(model2.getId()).thenReturn(realmId2);

        CreateRealmKey create1 = new CreateRealmKey(realmId, "rsa", "CN=test", 100L);
        CreateRealmKey create2 = new CreateRealmKey(realmId2, "rsa", "CN=test", 100L);

        controller.setRealmKey(model, create1);
        controller.setRealmKey(model2, create2);

        verify(model).addComponentModel(componentModelArgumentCaptor.capture());
        verify(model2).addComponentModel(componentModelArgumentCaptor.capture());

        List<ComponentModel> components = componentModelArgumentCaptor.getAllValues();

        ComponentModel comp1 = components.get(0);
        ComponentModel comp2 = components.get(1);

        assertNotEquals(comp1.getConfig().getFirst(PRIVATE_KEY_CONFIG_KEY), comp2.getConfig().getFirst(PRIVATE_KEY_CONFIG_KEY));
    }

    @Test
    void setRealmKey_previousPresent_reusesKey(RealmKeyController controller) {
        CreateRealmKey create1 = new CreateRealmKey(realmId, "rsa1", "CN=test", 100L);
        CreateRealmKey create2 = createRealmKeyFromEnv(null, null, "rsa2", 101L, true);

        controller.setRealmKey(model, create1);
        controller.setRealmKey(model, create2);

        verify(model, times(2)).addComponentModel(componentModelArgumentCaptor.capture());

        List<ComponentModel> components = componentModelArgumentCaptor.getAllValues();

        ComponentModel comp1 = components.get(0);
        ComponentModel comp2 = components.get(1);

        assertNotEquals(comp1.getName(), comp2.getName());
        assertEquals(comp1.getConfig().getFirst(PRIVATE_KEY_CONFIG_KEY), comp2.getConfig().getFirst(PRIVATE_KEY_CONFIG_KEY));
    }

    @Test
    void setRealmKey_suppliedOverrulesPrevious(RealmKeyController controller) {
        String privateKey = RandomStringUtils.randomAscii(50);
        String certificate = RandomStringUtils.randomAscii(50);

        CreateRealmKey create1 = new CreateRealmKey(realmId, "rsa1", "CN=test", 100L);
        CreateRealmKey create2 = createRealmKeyFromEnv(privateKey, certificate, "rsa2", 101L, true);


        controller.setRealmKey(model, create1);
        controller.setRealmKey(model, create2);

        verify(model, times(2)).addComponentModel(componentModelArgumentCaptor.capture());

        List<ComponentModel> components = componentModelArgumentCaptor.getAllValues();

        ComponentModel comp1 = components.get(0);
        ComponentModel comp2 = components.get(1);

        assertNotEquals(comp1.getConfig().getFirst(PRIVATE_KEY_CONFIG_KEY), comp2.getConfig().getFirst(PRIVATE_KEY_CONFIG_KEY));
    }

    @Test
    void setRealmKey_fromFilep12(RealmKeyController controller) {
        CreateRealmKey createRealmKey = createRealmKeyFromFile();

        controller.setRealmKey(model, createRealmKey);

        verify(model).addComponentModel(componentModelArgumentCaptor.capture());

        ComponentModel component = componentModelArgumentCaptor.getValue();


        System.out.println(component.getConfig().getFirst(PRIVATE_KEY_CONFIG_KEY));
        System.out.println(component.getConfig().getFirst(CERTIFICATE_CONFIG_KEY));
    }

    private CreateRealmKey createRealmKeyFromFile() {
        return new CreateRealmKey(
                realmId,
                "rsa",
                "CN=test",
                100,
                null,
                null,
                "./src/test/resources/certs/certificate.p12",
                "test".toCharArray(),
                "1",
                false
        );
    }
}
