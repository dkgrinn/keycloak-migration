package dk.grinn.keycloak.migration.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Spi
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.migration.entities.ScriptHistory;
import dk.grinn.keycloak.migration.entities.ScriptHistoryKey;
import dk.grinn.keycloak.migration.test.EntityManagerExtension;

import java.util.List;
import java.util.UUID;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.jupiter.api.AfterEach;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/**
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
@ExtendWith(EntityManagerExtension.class)
class RealmHistoryControllerIT {

    private String realm;

    private UUID sessionId;

    @BeforeEach
    void createSession(RealmHistoryLockController locks) {
        realm = RandomStringUtils.randomAlphabetic(10);
        sessionId = locks.createSession(realm, "realm history - test user");
        assertNotNull(sessionId);
    }

    @AfterEach
    void releaseSession(RealmHistoryLockController locks) {
        assertEquals(1, locks.releaseSession(sessionId));
    }

    @Test
    void testBeginMigration(RealmHistoryController controller) {
        ScriptHistoryKey key = controller.scriptBegin(sessionId, "V01.03", "Description", "JAVA", "test user");
        assertNotNull(key);
        controller.scriptCommit(sessionId, key, "Testing script", 42L, 420);
    }

    @Test
    void testOpenMigration(RealmHistoryController controller) {
        String version1 = "V01.03";
        ScriptHistoryKey key = controller.scriptBegin(sessionId, version1, "To be reponed", "JAVA", "test user");
        assertNotNull(key);
        controller.scriptCommit(sessionId, key, "Testing script", 42L, 420);

        ScriptHistoryKey reopnedKey = controller.scriptOpen(sessionId, version1, "test user");
        assertEquals(key, reopnedKey);
        controller.scriptCommit(sessionId, reopnedKey, "Testing script - reopened", 43L, 421);

        String version2 = "V01.03.9";
        ScriptHistoryKey key2 = controller.scriptBegin(sessionId, version2, "To be deleted", "JAVA", "test user");
        assertNotNull(key2);
        controller.scriptCommit(sessionId, key2, "Remove in i sec", 49L, 429);

        ScriptHistoryKey reopnedKey2 = controller.scriptOpen(sessionId, version2, "test user");
        assertNotNull(key2);
        controller.scriptRemove(sessionId, reopnedKey2);

        assertThrows(NoResultException.class, () -> controller.scriptOpen(sessionId, version2, "test user"));
    }

    @Test
    void testRemoveMigration(RealmHistoryController controller) {
        ScriptHistoryKey key = controller.scriptBegin(sessionId, "V42.03", "To be removed", "JAVA", "test user");
        assertNotNull(key);
        controller.scriptRemove(sessionId, key);
    }

    @Test
    void testRepeatableMigration(RealmHistoryController controller) {
        ScriptHistoryKey key = controller.scriptBegin(sessionId, "R01.07", "Repeatable script", "JAVA", "test user");
        assertNotNull(key);
        controller.scriptCommit(sessionId, key, "Testing script", 47L, 420);

        ScriptHistoryKey key2 = controller.scriptBegin(sessionId, "R01.07", "Repeatable script - second run", "JAVA", "test user", true);
        assertNotNull(key2);
        controller.scriptCommit(sessionId, key, "Testing script", 57L, 421);
    }

    @Test
    void testBeginMigrationWithOpenExceptions(RealmHistoryController controller) {
        ScriptHistoryKey key1 = controller.scriptBegin(sessionId, "V01.01", "Description", "JAVA", "test user");
        assertNotNull(key1);

        // Try to start a new script on the same session 
        assertThrows(NonUniqueResultException.class, () -> controller.scriptBegin(sessionId, "V01.02-to-fail", "Description", "JAVA", "test user"));

        // Close the first script and retry to start the second one again
        controller.scriptCommit(sessionId, key1, "Testing script (open version)", 43L, 430);
        assertNotNull(controller.scriptBegin(sessionId, "V01.02", "Description", "JAVA", "test user"));
    }

    /**
     * Test of getChecksums method, of class RealmHistoryController.
     */
    @Test
    void testGetHistory_UUID(RealmHistoryController controller) {
        testBeginMigrationWithOpenExceptions(controller);
        List<ScriptHistory> history = controller.getHistory(sessionId);
        assertNotNull(history);
        assertEquals(2, history.size());
        assertNotNull(history.get(0).getScript());
        assertNull(history.get(1).getScript());
    }

    @Test
    void testGetHistory_String(RealmHistoryController controller) {
        testBeginMigrationWithOpenExceptions(controller);
        List<ScriptHistory> history = controller.getHistory(realm);
        assertNotNull(history);
        assertEquals(2, history.size());
        assertNotNull(history.get(0).getScript());
        assertNull(history.get(1).getScript());
    }

}
