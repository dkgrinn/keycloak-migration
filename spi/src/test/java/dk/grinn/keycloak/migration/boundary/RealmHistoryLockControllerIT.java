package dk.grinn.keycloak.migration.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Spi
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.migration.entities.RealmHistoryLock;
import dk.grinn.keycloak.migration.test.EntityManagerExtension;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import java.util.UUID;
import java.util.concurrent.Exchanger;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static java.lang.System.out;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
@ExtendWith(EntityManagerExtension.class)
class RealmHistoryLockControllerIT {

    RealmHistoryLockControllerIT() {
    }

    @Test
    void testGetAllLocks(RealmHistoryLockController controller) {
        assertTrue(controller.getAll().stream()
                .anyMatch((lock) -> lock.getId() == 1 && lock.getRealm().equals("master")
                ));
    }

    @Test
    void testGetSessions(RealmHistoryLockController controller) {
        assertTrue(controller.getSessions(false).stream()
                .anyMatch((lock) -> lock.getRealm().equals("master")
                ));
    }

    @Test
    void testCreateSession(RealmHistoryLockController controller) {
        String realm = RandomStringUtils.randomAlphabetic(10);
        UUID sessionId = controller.createSession(realm, "create session user");
        assertNotNull(sessionId);

        // Retry creating a session on the same realm - should fail
        assertThrows(OptimisticLockException.class, () -> controller.createSession(realm, "retry create session user"));
    }

    @Test
    void testReleaseSession(RealmHistoryLockController controller) {
        String realm = RandomStringUtils.randomAlphabetic(10);
        UUID sessionId = controller.createSession(realm, "release session user1");
        assertNotNull(sessionId);

        // Retry creating a session on the same realm - should fail
        assertThrows(OptimisticLockException.class, () -> controller.createSession(realm, "retry release session user1"));

        // Release it and retry creating a session
        assertEquals(1, controller.releaseSession(sessionId));
        assertNotNull(controller.createSession(realm, "release session user2"));
    }

    @Test
    void testAbortSession(RealmHistoryLockController controller) {
        String realm = RandomStringUtils.randomAlphabetic(10);
        UUID sessionId = controller.createSession(realm, "abort session user1");
        assertNotNull(sessionId);

        // Retry creating a session on the same realm - should fail
        assertThrows(OptimisticLockException.class, () -> controller.createSession(realm, "test user"));

        // Abort it and retry creating a session
        assertEquals(1, controller.abortSession(realm, "abort session user1"));
        assertNotNull(controller.createSession(realm, "abort session user2"));
    }

    @Test
    void testRemoveSession(RealmHistoryLockController controller) {
        String realm = RandomStringUtils.randomAlphabetic(10);
        UUID sessionId = controller.createSession(realm, "remove session user");
        assertNotNull(sessionId);

        assertNotNull(controller.get(sessionId));
        controller.removeSession(sessionId);
        assertThrows(NoResultException.class, () -> controller.get(sessionId));
    }

    /**
     * Test of lockMaster method.It's a bit complex as we need to use two
     * threads and two entity managers in order to verify that a lock on the
     * master entry will block other locks until released (committed).
     */
    @Test
    void testLockMaster(RealmHistoryLockController controller1, EntityManagerFactory factory) throws InterruptedException, TimeoutException {
        EntityManager em2 = factory.createEntityManager();
        em2.getTransaction().begin();
        try {
            RealmHistoryLockController controller2 = new RealmHistoryLockController(em2);
            out.println("controller2 - trying to get a lock on the master");
            RealmHistoryLock lockMaster2 = controller2.lockMaster();
            assertNotNull(lockMaster2);
            out.println("controller2 - got the lock");

            Exchanger<RealmHistoryLock> exchanger = new Exchanger<>();
            Thread t2 = new Thread(() -> {
                out.println("controller2 - exchange with controller1");
                assertThrows(TimeoutException.class, () -> exchanger.exchange(lockMaster2, 1, TimeUnit.SECONDS));
                out.println("controller2 - timeout (as expected) - releasing lock");
                em2.getTransaction().commit();
                out.println("controller2 - trying to exchange with controller1 again");
                try {
                    exchanger.exchange(lockMaster2, 1, TimeUnit.SECONDS);
                } catch (InterruptedException | TimeoutException ex) {
                    fail("Didn't expect a timeout");
                }
                out.println("controller2 - successfull exchange");
            });
            t2.start();

            out.println("controller1 - also trying to get a lock on the master");
            RealmHistoryLock lockMaster1 = controller1.lockMaster();
            assertNotNull(lockMaster1);
            out.println("controller1 - got the lock - exhanging it with controller2");
            exchanger.exchange(lockMaster1, 4, TimeUnit.SECONDS);
            out.println("controller1 - successfull exchange");
        } finally {
            em2.close();
        }
    }

}
