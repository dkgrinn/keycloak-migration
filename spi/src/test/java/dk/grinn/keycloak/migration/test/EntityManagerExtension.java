package dk.grinn.keycloak.migration.test;

/*-
 * #%L
 * Keycloak : Migrate : Spi
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.hibernate.Session;
import org.hibernate.internal.SessionFactoryImpl;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Properties;

import static java.nio.file.Files.newInputStream;

/**
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
public class EntityManagerExtension implements BeforeAllCallback, AfterAllCallback, BeforeEachCallback, AfterEachCallback, ParameterResolver {

    private EntityManagerFactory factory;

    private EntityManager em;

    @Override
    public void beforeAll(ExtensionContext ec) throws Exception {
        // Read configuration
        Properties conf = new Properties();
        try (InputStream is = newInputStream(Paths.get("../build.properties"))) {
            conf.load(is);
        }
        final var props = new HashMap<String, String>();
        props.put("javax.persistence.jdbc.user", conf.getProperty("liquibase.username"));
        props.put("javax.persistence.jdbc.password", conf.getProperty("liquibase.password"));
        props.put("javax.persistence.jdbc.url", conf.getProperty("liquibase.url"));
        props.put("javax.persistence.jdbc.driver", conf.getProperty("liquibase.driver"));

        factory = Persistence.createEntityManagerFactory("TestPU", props);
        em = factory.createEntityManager();

        // Get connection from entitymanager
        final var session = em.unwrap(Session.class);

        session.doWork(connection -> {
            // Get liquibase database
            try {
                Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
                final var liquibase = new Liquibase("META-INF/dk/grinn/keycloak/migrate/changelog.xml", new ClassLoaderResourceAccessor(), database);
                liquibase.update("test");
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        });


    }

    @Override
    public void afterAll(ExtensionContext ec) {
        em.close();
        factory.close();
    }

    @Override
    public void beforeEach(ExtensionContext context) {
        em.getTransaction().begin();
    }

    @Override
    public void afterEach(ExtensionContext context) {
        if (context.getExecutionException().isEmpty()) {
            em.getTransaction().commit();
        } else {
            em.getTransaction().rollback();
        }
        em.clear();
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        if (parameterContext.getParameter().getType().equals(EntityManagerFactory.class)) {
            return true;
        } else {
            for (Constructor<?> constructor : parameterContext.getParameter().getType().getConstructors()) {
                Class<?>[] parameterTypes = constructor.getParameterTypes();
                if (parameterTypes.length == 1) {
                    if (parameterTypes[0].isAssignableFrom(EntityManager.class)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        if (parameterContext.getParameter().getType().equals(EntityManagerFactory.class)) {
            return factory;
        } else {
            try {
                return parameterContext.getParameter().getType().getConstructor(EntityManager.class).newInstance(em);
            } catch (SecurityException | ReflectiveOperationException ex) {
                throw new ParameterResolutionException("Unable to create a new object", ex);
            }
        }
    }

}
