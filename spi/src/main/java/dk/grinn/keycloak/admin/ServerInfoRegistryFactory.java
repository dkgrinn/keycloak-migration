package dk.grinn.keycloak.admin;

/*-
 * #%L
 * Keycloak : Migrate : Spi
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import org.eclipse.microprofile.health.HealthCheck;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.spi.ObjectFactory;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Collections.unmodifiableList;

/**
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
public class ServerInfoRegistryFactory implements ObjectFactory {

    @SuppressWarnings("unused")
    public static final String JNDI_NAME = "java:global/migration/server-info";

    private final Map<Object, ServerInfoRegistry> cache = new ConcurrentHashMap<>();

    @Override
    public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable<?, ?> environment) {
        return cache.computeIfAbsent(obj, key -> new ServerInfoRegistryImpl());
    }

    private static class ServerInfoRegistryImpl implements ServerInfoRegistry {

        private final List<HealthCheck> healthChecks = new ArrayList<>();

        @Override
        public void register(HealthCheck healthCheck) {
            healthChecks.add(healthCheck);
        }

        @Override
        public void unRegister(HealthCheck healthCheck) {
            healthChecks.remove(healthCheck);
        }

        @Override
        public List<HealthCheck> getAll() {
            return unmodifiableList(healthChecks);
        }
    }
}
