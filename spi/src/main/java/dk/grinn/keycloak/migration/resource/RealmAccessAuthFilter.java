package dk.grinn.keycloak.migration.resource;

/*-
 * #%L
 * Keycloak : Migrate : Spi
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import org.jboss.logging.Logger;
import org.jboss.resteasy.core.ResourceMethodInvoker;
import org.jboss.resteasy.spi.HttpRequest;
import org.keycloak.models.KeycloakContext;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RoleModel;
import org.keycloak.models.UserModel;
import org.keycloak.representations.AccessToken;
import org.keycloak.services.managers.AppAuthManager;
import org.keycloak.services.managers.AuthenticationManager;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static java.util.Arrays.asList;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

/**
 *
 */
public class RealmAccessAuthFilter implements ContainerRequestFilter {

    private static final Logger LOG = Logger.getLogger(RealmAccessAuthFilter.class);

    /**
     * Name of the request context property assigned the auth result to be used
     * for authentication.
     *
     * @see AuthenticationManager.AuthResult
     */
    private static final String AUTH_REQUEST = RealmAccessAuthFilter.class.getName().concat(".AuthRequest");

    /**
     * Mark the current session context as authentication required.
     */
    public static void requireAuthentication(KeycloakSession session, AppAuthManager appAuthManager, List<String> realms) {
        session.getContext().getContextObject(HttpRequest.class)
                .setAttribute(AUTH_REQUEST, new AuthRequest(session, appAuthManager, realms));
    }

    public static AuthenticationManager.AuthResult getAuthResult(KeycloakSession session) {
        return ((AuthRequest) session.getContext().getContextObject(
                HttpRequest.class).getAttribute(AUTH_REQUEST)).auth;
    }

    @Override
    public void filter(ContainerRequestContext requestContext) {
        // Check roles if auth has been required by the RealmResourceProvider implementation
        AuthRequest authRequest;
        if ((authRequest = (AuthRequest) requestContext.getProperty(AUTH_REQUEST)) != null) {
            authRequest.getAuth(requestContext)
                    .ifPresent(auth -> checkRoles(auth, requestContext));
        }
    }

    private void checkRoles(AuthenticationManager.AuthResult auth, ContainerRequestContext requestContext) {
        ResourceMethodInvoker methodInvoker = (ResourceMethodInvoker) requestContext.getProperty("org.jboss.resteasy.core.ResourceMethodInvoker");
        Method method = methodInvoker.getMethod();

        if (!method.isAnnotationPresent(PermitAll.class)) {
            if (!method.isAnnotationPresent(DenyAll.class)) {
                checkRoles(auth, method)
                        .ifPresent(response -> requestContext.abortWith(response.build()));
            } else {
                requestContext.abortWith(Response.status(SC_UNAUTHORIZED).build());
            }
        }
    }

    private Optional<Response.ResponseBuilder> checkRoles(AuthenticationManager.AuthResult auth, Method resourceMethod) {
        Set<String> accessRoles = new HashSet<>();
        Class<?> resourceClass = resourceMethod.getDeclaringClass();

        // Get the annotated roles
        if (resourceMethod.isAnnotationPresent(RolesAllowed.class)) {
            accessRoles.addAll(asList(resourceMethod.getAnnotation(RolesAllowed.class).value()));
        } else if (resourceClass.isAnnotationPresent(RolesAllowed.class)) {
            accessRoles.addAll(asList(resourceClass.getAnnotation(RolesAllowed.class).value()));
        } else if (resourceClass.isAnnotationPresent(PermitAll.class)) {
            // Class level permit all
            // And no deny all or roles allowed on either method or class
            return Optional.empty();
        }

        // The actual check
        if (auth.getToken() != null && auth.getToken().getRealmAccess() != null) {
            // Check against the bearer token
            return checkRoles(accessRoles, auth.getToken());
        } else {
            // Check against the cookie user
            return checkRoles(accessRoles, auth.getUser());
        }
    }

    private Optional<Response.ResponseBuilder> checkRoles(Set<String> accessRoles, AccessToken token) {
        Optional<String> firstMatchedRole = accessRoles.stream()
                .filter(token.getRealmAccess()::isUserInRole)
                .findFirst();
        if (firstMatchedRole.isPresent()) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("First matched role: " + firstMatchedRole.get());
            }
            return Optional.empty();
        }
        return Optional.of(Response.status(SC_UNAUTHORIZED));
    }

    private Optional<Response.ResponseBuilder> checkRoles(Set<String> accessRoles, UserModel user) {
        Optional<String> firstMatchedRole = user.getRealmRoleMappingsStream()
                .map(RoleModel::getName)
                .filter(accessRoles::contains)
                .findFirst();
        if (firstMatchedRole.isPresent()) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("First matched role: " + firstMatchedRole.get());
            }
            return Optional.empty();
        }
        return Optional.of(Response.status(SC_UNAUTHORIZED));
    }

    /**
     *
     */
    private static class AuthRequest {

        final KeycloakSession session;
        final AppAuthManager appAuthManager;
        final List<String> realms;
        AuthenticationManager.AuthResult auth = null;

        AuthRequest(KeycloakSession session, AppAuthManager appAuthManager, List<String> realms) {
            this.appAuthManager = appAuthManager;
            this.session = session;
            this.realms = realms;
        }

        Optional<AuthenticationManager.AuthResult> getAuth(ContainerRequestContext requestContext) {
            KeycloakContext context = session.getContext();

            if (realms.contains(context.getRealm().getName())) {
                auth = new AppAuthManager.BearerTokenAuthenticator(session).authenticate();
                if (auth != null) {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Bearer auth: " + auth.getToken().getPreferredUsername());
                    }
                } else {
                    auth = appAuthManager.authenticateIdentityCookie(session, context.getRealm());
                    if (auth != null && LOG.isDebugEnabled()) {
                        LOG.debug("Cookie auth: " + auth.getUser().getUsername());
                    }
                }
            }
            if (auth == null) {
                requestContext.abortWith(Response.status(SC_UNAUTHORIZED).build());
            }
            return Optional.ofNullable(auth);
        }

    }
}
