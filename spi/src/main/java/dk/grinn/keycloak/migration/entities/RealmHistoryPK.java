package dk.grinn.keycloak.migration.entities;

/*-
 * #%L
 * Keycloak : Migrate : Spi
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
@SuppressWarnings("unused")
@Embeddable
public class RealmHistoryPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id", updatable = false)
    private int id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "installed_rank")
    private int installedRank;

    public RealmHistoryPK() {
    }

    public RealmHistoryPK(ScriptHistoryKey key) {
        this(key.getId(), key.getRank());
    }
    
    public RealmHistoryPK(int id, int installedRank) {
        this.id = id;
        this.installedRank = installedRank;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getInstalledRank() {
        return installedRank;
    }
    
    public ScriptHistoryKey asScriptHistoryKey() {
        return new ScriptHistoryKey(id, installedRank);
    }

    public void setInstalledRank(int installedRank) {
        this.installedRank = installedRank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RealmHistoryPK)) return false;

        RealmHistoryPK that = (RealmHistoryPK) o;

        if (id != that.id) return false;
        return installedRank == that.installedRank;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + installedRank;
        return result;
    }

    @Override
    public String toString() {
        return "dk.grinn.keycloak.migration.entities.GkcadmRealmHistoryPK[ id=" + id + ", installedRank=" + installedRank + " ]";
    }
    
}
