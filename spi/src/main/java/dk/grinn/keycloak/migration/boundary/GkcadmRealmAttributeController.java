package dk.grinn.keycloak.migration.boundary;

/*-
 * #%L
 * Keycloak : Migrate : Spi
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.migration.entities.GkcadmRealmAttribute;
import dk.grinn.keycloak.migration.services.GkcadmRealmAttributeService;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 * 
 * @author @author Nicolai Gjøderum &lt;nig-trifork@gmail.com&gt;
 */
public class GkcadmRealmAttributeController implements GkcadmRealmAttributeService {

    protected final EntityManager em;

    @Inject
    public GkcadmRealmAttributeController(EntityManager em){
        this.em = em;
    }

    @Override
    public String getAttribute(String realmName, String name) {
        GkcadmRealmAttribute attr = get(realmName, name);

        return attr != null ? attr.getValue() : null;
    }

    @Override
    public String putAttribute(String realmName, String name, String value) {
        String previousValue = null;

        GkcadmRealmAttribute attr = get(realmName, name);

        if(attr == null){
            attr = new GkcadmRealmAttribute(realmName, name, value);
            em.persist(attr);
        } else {
            previousValue = attr.getValue();
            attr.setValue(value);
            em.merge(attr);
        }

        em.flush();
        return previousValue;
    }

    private GkcadmRealmAttribute get(String realmName, String name){
        TypedQuery<GkcadmRealmAttribute> query = em.createNamedQuery("GkcadmRealmAttribute.findByRealmAndName", GkcadmRealmAttribute.class);
        query.setParameter("name", name);
        query.setParameter("realmName", realmName);

        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
