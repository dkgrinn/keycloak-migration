package dk.grinn.keycloak.migration.entities;

/*-
 * #%L
 * Keycloak : Migrate : Spi
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
@Entity
@Table(name = "gkcadm_realm_history_lock")
@NamedQuery(name = "RealmHistoryLock.findAll",
        query = "SELECT rh FROM RealmHistoryLock rh"
)
@NamedQuery(name = "RealmHistoryLock.findSessions",
        query = "SELECT new dk.grinn.keycloak.migration.entities.Session("
                + "     rh.realm, rh.session, rh.lockGranted, rh.lockedBy"
                + ") FROM RealmHistoryLock rh "
                + "where :onlyActive = false or rh.session is not null"
)
@NamedQuery(name = "RealmHistoryLock.findByRealm",
        query = "SELECT rh "
                + "FROM RealmHistoryLock rh "
                + "where rh.realm = :realm"
)
@NamedQuery(name = "RealmHistoryLock.findBySession",
        query = "SELECT rh "
                + "FROM RealmHistoryLock rh "
                + "where rh.session = :session"
)
@NamedQuery(name = "RealmHistoryLock.releaseSession",
        query = "UPDATE RealmHistoryLock rh "
                + "SET rh.session = null, rh.lockGranted = null, rh.lockedBy = null "
                + "where rh.session = :session"
)
@NamedQuery(name = "RealmHistoryLock.releaseSessionByRealm",
        query = "UPDATE RealmHistoryLock rh "
                + "SET rh.session = null, rh.lockGranted = null, rh.lockedBy = null "
                + "where rh.realm = :realm and rh.session is not null"
)
public class RealmHistoryLock implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, updatable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "realm")
    private String realm;

    @Column(name = "session")
    private byte[] session;

    @Column(name = "lock_granted")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lockGranted;

    @Column(name = "locked_by")
    private String lockedBy;

    @Basic(optional = false)
    @Column(name = "current_rank")
    private int currentRank;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "historyLock", fetch = FetchType.LAZY)
    @Column(name = "history")
    private Collection<RealmHistory> history;

    public RealmHistoryLock() {
    }

    public RealmHistoryLock(String realm, String lockedBy) {
        this.realm = realm;
        this.lockedBy = lockedBy;
        this.lockGranted = new Date();
        this.session = new byte[16];
        this.history = new ArrayList<>();

        UUID uuId = UUID.randomUUID();
        ByteBuffer bb = ByteBuffer.wrap(this.session);
        bb.putLong(uuId.getMostSignificantBits());
        bb.putLong(uuId.getLeastSignificantBits());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRealm() {
        return realm;
    }

    public void setRealm(String realm) {
        this.realm = realm;
    }

    public UUID getSession() {
        if (session != null) {
            ByteBuffer bb = ByteBuffer.wrap(session);
            return new UUID(bb.getLong(), bb.getLong());
        }
        return null;
    }

    public void setSession(String user) {
        setSession(UUID.randomUUID());
        setLockedBy(user);
        setLockGranted(new Date());
    }

    public void setSession(UUID session) {
        this.session = new byte[16];
        ByteBuffer bb = ByteBuffer.wrap(this.session);
        bb.putLong(session.getMostSignificantBits());
        bb.putLong(session.getLeastSignificantBits());
    }

    public Date getLockGranted() {
        return lockGranted;
    }

    public void setLockGranted(Date lockGranted) {
        this.lockGranted = lockGranted;
    }

    public String getLockedBy() {
        return lockedBy;
    }

    public void setLockedBy(String lockedBy) {
        this.lockedBy = lockedBy;
    }

    public int getCurrentRank() {
        return currentRank;
    }

    public void setCurrentRank(int currentRank) {
        this.currentRank = currentRank;
    }

    public int incrementAndGetCurrentRank() {
        return ++currentRank;
    }

    public Collection<RealmHistory> getHistory() {
        return history;
    }

    public void setHistory(Collection<RealmHistory> history) {
        this.history = history;
    }

    public Session toSession() {
        return new Session(realm, session, lockGranted, lockedBy);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RealmHistoryLock)) return false;

        RealmHistoryLock that = (RealmHistoryLock) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "dk.grinn.keycloak.migration.entities.RealmHistoryLock[ id=" + id + " ]";
    }

}
