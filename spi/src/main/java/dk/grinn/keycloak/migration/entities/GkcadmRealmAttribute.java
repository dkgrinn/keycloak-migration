package dk.grinn.keycloak.migration.entities;

/*-
 * #%L
 * Keycloak : Migrate : Spi
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 
 * @author @author Nicolai Gjøderum &lt;nig-trifork@gmail.com&gt;
 */
@SuppressWarnings("unused")
@Entity
@Table(name = "gkcadm_realm_attribute")
@NamedQueries(
        @NamedQuery(name = "GkcadmRealmAttribute.findByRealmAndName",
                query = "SELECT gra " +
                "FROM GkcadmRealmAttribute gra " +
                "WHERE gra.name = :name AND gra.realmName = :realmName")
)
public class GkcadmRealmAttribute implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", insertable = false, updatable = false)
    private String id;

    @Column(name = "realm_name")
    private String realmName;

    @Column(name = "name")
    private String name;

    @Column(name = "value")
    private String value;

    public GkcadmRealmAttribute() {
    }

    public GkcadmRealmAttribute(String realmName, String name, String value) {
        this.realmName = realmName;
        this.name = name;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public String getRealmName() {
        return realmName;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setRealmName(String realmName) {
        this.realmName = realmName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof GkcadmRealmAttribute)) return false;

        GkcadmRealmAttribute other = (GkcadmRealmAttribute) o;

        return (this.id == null && other.id == null) || (this.id != null && this.id.equals(other.id));
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
}
