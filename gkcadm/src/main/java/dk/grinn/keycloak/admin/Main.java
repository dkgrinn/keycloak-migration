package dk.grinn.keycloak.admin;

import dk.grinn.keycloak.migration.KeycloakMigrate;
import dk.grinn.keycloak.migration.annotation.Migration;
import dk.grinn.keycloak.migration.core.JavaMigration;
import dk.grinn.keycloak.migration.core.LogOutputStream;
import org.apache.commons.configuration2.CompositeConfiguration;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.ParserProperties;

import java.io.IOException;
import java.io.PrintStream;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static java.lang.String.format;
import static java.lang.System.err;
import static java.lang.System.exit;
import static java.lang.System.setProperty;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

/*-
 * #%L
 * Keycloak : Admin
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

/**
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
public class Main {

    private static final Logger LOG;

    private static final Weld weld;

    static {
        weld = initWeld();
        LOG = initLogger();
    }

    @SuppressWarnings("unchecked")
    private static Weld initWeld() {
        var weld = new Weld();
        weld.addBeanDefiningAnnotations(Migration.class);
        return weld;
    }

    /**
     * Logging setup:
     * <p>
     * This is a CLI application with console output. So no need for a complex
     * logging setup. We mainly need Weld to go quite on startup. The builtin
     * Java util logging is adequate for this task.
     */
    private static Logger initLogger() {
        try (var is = Main.class.getClassLoader().getResourceAsStream("logging.properties")) {
            LogManager.getLogManager().readConfiguration(is);
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
        return Logger.getLogger(Main.class.getName());
    }

    @Option(name = "-l", aliases = "--locations", usage = "(Optional) Comma list of configured locations.")
    private String locations;

    @Option(name = "-c", aliases = "--configFiles", usage = "(Optional) Comma list of configuration files.")
    private String configFiles;

    @Option(name = "-i", aliases = "--ignoreWarnings", usage = "Ignore any configuration warnings.")
    private boolean ignoreWarnings;

    @Option(name = "-o", aliases = "--output", metaVar = "dest", usage = "Destination of console output ('stdout', 'stderr' or 'logger')")
    private String output = "stdout";

    @Argument
    private List<String> commands = new ArrayList<>();


    /**
     * Application entry point.
     */
    protected void doMain() {
        // Optional config files set as property
        if (configFiles != null) {
            setProperty("configFiles", configFiles);
        }

        // Check the commands validity
        checkValidCommands("migrate", "info", "clean", "abort", "dry-run");

        // Start the CDI container
        try (var container = weld.initialize()) {
            configureOutput(container);

            // Execute the commands for each realm of each location
            for (var location : locations(container)) {
                for (var realm : realmsByLocation(location, container)) {
                    container.select(KeycloakMigrate.class).get()
                            .execute(location, realm, commands);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace(err);
            exit(1);
        }
    }

    protected void configureOutput(WeldContainer container) {
        var appConfig = container.select(ApplicationConfiguration.class).get();
        switch (output) {
            case "stdout":
                appConfig.setOutput(System.out);
                break;
            case "stderr":
                appConfig.setOutput(System.err);
                break;
            case "logger":
                appConfig.setOutput(new PrintStream(new LogOutputStream(), true));
                break;
            default:
                throw new IllegalArgumentException("Unknown output destination: " + output);
        }
    }

    /**
     * Application entry point called from outside.
     * <p>
     * Responsible of setting up the command line parser and instantiating and
     * running the main execution object (this class).
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Main app, parse commandline and run
        Main app = new Main();
        CmdLineParser parser = new CmdLineParser(app, ParserProperties.defaults().withUsageWidth(120));
        try {
            parser.parseArgument(args);
            app.doMain();
        } catch (Exception ex) {
            err.println(ex.getMessage());
            err.println("Usage: gkcadm [options] migrate|clean|info arguments....");
            parser.printUsage(err);
            exit(1);
        }
    }

    /**
     * Get all locations.
     * <p>
     * Validates the configuration:<ul>
     * <li> Locations exists (if not, error)
     * <li> Referenced scopes does exists in at least one migration (if not,
     * error)
     * <li> Defined migration scopes are actually used (if not, warning)
     * </ul>
     */
    private List<String> locations(WeldContainer container) {
        // The full configuration
        var config = container.select(CompositeConfiguration.class).get();

        // Two possible sources
        List<String> result;
        if (locations != null) {
            // From command line (overrules config file definition)
            result = Arrays.stream(locations.split(","))
                    .map(String::trim)
                    .collect(toList());
        } else {
            // As defined in the configuration
            result = config.getList(String.class, "gkcadm.locations");
        }

        // Validate that locations has a configuration
        var configScopes = new HashSet<String>();
        for (var location : result) {
            if (!config.containsKey(format("gkcadm.location.%s.realms", location))) {
                LOG.severe(() -> format("No configuration for location: '%s'", location));
            } else {
                configScopes.addAll(
                        config.getList(String.class, format("gkcadm.location.%s.scopes", location), emptyList())
                );
            }
        }

        // Check that defined migrations are actually used.
        var definedScopes = new HashSet<String>();
        for (var handler : container.select(JavaMigration.class).handlers()) {
            var migration = handler.getBean().getBeanClass().getAnnotation(Migration.class);
            if (migration != null && !migration.scope().isBlank()) {
                if (configScopes.contains(migration.scope())) {
                    definedScopes.add(migration.scope());
                } else {
                    LOG.warning(() -> format("Migration defined but never used: '%s' (scope=\"%s\")",
                            handler.getBean().getBeanClass(), migration.scope())
                    );
                }
            }
        }

        configScopes.removeAll(definedScopes);
        if (!configScopes.isEmpty()) {
            LOG.warning(() -> format("Scopes referenced but not defined in any migration: %s", configScopes));
        }
        return result;
    }

    private List<String> realmsByLocation(String location, WeldContainer container) {
        var config = container.select(CompositeConfiguration.class).get();
        var key = format("gkcadm.location.%s.realms", location);

        return config.containsKey(key)
                ? asList(config.getStringArray(key))
                : emptyList();
    }

    private void checkValidCommands(String... validCommands) {
        if (commands.isEmpty()) {
            throw new IllegalArgumentException("command is empty");
        }
        List<String> invalidCommands = new ArrayList<>(commands);
        invalidCommands.removeAll(asList(validCommands));
        if (!invalidCommands.isEmpty()) {
            throw new IllegalArgumentException("Unknown command: " + invalidCommands.get(0));
        }
    }

}
