package dk.grinn.keycloak.migration;

/*-
 * #%L
 * Keycloak : Migrate : Core
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import dk.grinn.keycloak.admin.GkcadmConfiguration;
import dk.grinn.keycloak.admin.ProgressStream;
import dk.grinn.keycloak.migration.boundary.CreateSession;
import dk.grinn.keycloak.migration.boundary.SessionResource;
import dk.grinn.keycloak.migration.core.ClientMigrationContext;
import dk.grinn.keycloak.migration.core.migrate.MigratorService;
import dk.grinn.keycloak.migration.core.rest.Create;
import dk.grinn.keycloak.migration.entities.ScriptHistory;
import org.jboss.logging.Logger;
import org.jboss.weld.context.activator.ActivateRequestContext;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Main migration class.
 * <p>
 */
@Dependent
public class KeycloakMigrate {

    public static final String ENVIRONMENT_CONFIG_PREFIX = "GKCADM_";

    private static final Logger LOG = Logger.getLogger(KeycloakMigrate.class);

    @Inject
    protected Instance<MigratorService> migrator;

    @Inject
    protected GkcadmConfiguration configuration;

    @Inject
    protected ClientMigrationContext context;

    @Inject
    protected ProgressStream out;

    @PostConstruct
    public void init() {
        LOG.debug("Init:");
    }

    /**
     * Initialize and scan the class,- and script locations for migrations.
     *
     */
    @ActivateRequestContext
    public void execute(String location, String realm, List<String> commands) throws Exception {
        LOG.debugf("Location: %s, commands: %s", location, commands);
        configuration.setLocation(location);

        for (String cmd : commands) {
            switch (cmd) {
                case "info":
                    info(realm);
                    break;
                case "migrate":
                    migrate(realm);
                    break;
                case "clean":
                    clean(realm);
                    break;
                case "abort":
                    abort(realm);
                    break;
                default:
                    LOG.warnf("Unknown command '%s'", cmd);
            }
        }
    }

    /**
     */
    protected void migrate(String realm) throws Exception {
        // New migrator service for each realm
        migrator.get().migrate(realm, configuration);
    }

    protected void clean(String realm) {
        SessionResource sessions = context.migrations().sessions();
        out.println("--------------------------------");

        UUID sessionId = Create.uuid(
                sessions.createSession(new CreateSession(realm))
        );
        try {
            context.keycloak().realm(realm).remove();
            out.printf("Cleaned realm: '%s'\n", realm);
        } catch (NotFoundException ex) {
            // Ignore
        } finally {
            sessions.removeSession(sessionId);
        }
    }

    protected void info(String realm) {
        List<ScriptHistory> history = null;
        try {
            history = context.migrations().realms().scripts(realm).getScripts();
        } catch (NotFoundException ex) {
            // Ignore
        }

        out.printf("\nRealm: '%s'\n", realm);
        out.println("--------------------------------");
        if (history != null) {
            migrator.get().info(realm, configuration, history);
        } else {
            out.println("(Not under migration control)");
        }
    }

    protected void abort(String realm) {
        out.printf("\nAbort: '%s'\n", realm);
        context.migrations().realms().abortSession(realm);
    }

    public Set<String> getNotDefinedConfigEnvironmentKeys(Set<String> envKeys) {
        return envKeys.stream()
                .filter(key -> key.startsWith(ENVIRONMENT_CONFIG_PREFIX))
                .filter(this::isNotInConfiguration)
                .collect(Collectors.toSet());
    }

    private boolean isNotInConfiguration(String key) {
        final var configKey = key.substring(ENVIRONMENT_CONFIG_PREFIX.length()).toLowerCase().replace('_', '.');
        return !configuration.containsKey(configKey);
    }
}
