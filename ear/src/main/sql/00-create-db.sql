CREATE USER IF NOT EXISTS 'keycloak'@'%' IDENTIFIED BY '<password>';
CREATE DATABASE keycloak_fut CHARACTER SET utf8 COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON keycloak_fut.* TO 'keycloak'@'%';
FLUSH PRIVILEGES;
