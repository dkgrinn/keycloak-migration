@REM
@REM Script inspired from flyway
@REM https://github.com/flyway/flyway/blob/master/flyway-commandline/src/main/assembly/flyway.cmd
@REM

@Echo off

setlocal

@REM Set the current directory to the installation directory
call :getCurrentBatch INSTALLDIR1
set INSTALLDIR=%INSTALLDIR1%
set INSTALLDIR=%INSTALLDIR:~0,-10%

if exist "%INSTALLDIR%\jre\bin\java.exe" (
 set JAVA_CMD="%INSTALLDIR%\jre\bin\java.exe"
) else (
 @REM Use JAVA_HOME if it is set
 if "%JAVA_HOME%"=="" (
  set JAVA_CMD=java
 ) else (
  set JAVA_CMD="%JAVA_HOME%\bin\java.exe"
 )
)

SET CP=
IF DEFINED CLASSPATH ( SET CP=%CLASSPATH%;)

if "%JAVA_ARGS%"=="" (
  set JAVA_ARGS=
)

%JAVA_CMD% %JAVA_ARGS% -cp "%CP%%INSTALLDIR%\lib\*;%INSTALLDIR%\jars\*" dk.grinn.keycloak.admin.Main %*
EXIT /B %ERRORLEVEL%

:getCurrentBatch variableName
    set "%~1=%~f0"
    goto :eof